import 'reflect-metadata';
import {
  Controller,
  UseFilters,
  UseGuards,
  Render,
  Get,
  Req,
  Post,
  Body,
  Res,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthExceptionFilter } from '@shared/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '@shared/guards/authenticated.guard';
import { DataTableQuery } from '@decorators/datatable_query.decorator';
import { AuthSuperAdminGuard } from '@shared/guards/authSuperAdmin.guard';
import { ApiFormExceptionFilter } from '@shared/filters/api.form-exceptions.filter';
import { Response } from 'express';
import { env } from '@env';
import { SalariesService } from './salary.service';
import { CreateSalariesDto } from './salary.dto';

@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
@UseGuards(AuthSuperAdminGuard)
@Controller('salaries')
export class SalariesController {
  constructor(private readonly SalariesService: SalariesService) {}

  @Get('/datatable/:employeeId')
  async getAllSalariess(
    @DataTableQuery() query,
    @Param('employeeId') employeeId: number,
  ) {
    query.filter = { employee_id: employeeId };
    return await this.SalariesService.findAllSalariesOfEmployee(query);
  }

  @Post('/create')
  @UseFilters(ApiFormExceptionFilter)
  async createSalaries(
    @Body() createSalariesDto: CreateSalariesDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.SalariesService.createSalaries(createSalariesDto);

      return res.json({
        status: 'success',
        message: 'Check in success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/delete/:id')
  @UseFilters(ApiFormExceptionFilter)
  async deleteSalariesById(
    @Param('id', ParseIntPipe) id: number,
    @Res() res: Response,
  ) {
    await this.SalariesService.deleteSalary(id);
    return res.json({
      status: 'success',
      message: 'Delete Check in success',
    });
  }

  @Get('/exportSalary/:id')
  async ExportByCampaign(
    @Param('id', ParseIntPipe) id: number,
    @Res() res: Response,
  ) {
    try {
      const data = await this.SalariesService.exportByEmployeeId(id);

      return res.json(data);
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }
}
