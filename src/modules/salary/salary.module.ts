import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { SalariesRepository } from './salary.repository';
import { SalariesEntity } from '@entity';
import { SalariesController } from './salary.controller';
import { SalariesService } from './salary.service';

@Module({
  imports: [TypeOrmModule.forFeature([SalariesEntity])],
  providers: [SalariesService, SalariesRepository],
  controllers: [SalariesController],
  exports: [SalariesService],
})
export class SalariesModule {}
