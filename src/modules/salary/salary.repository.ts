import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { SalariesEntity } from '@entity';

@Injectable()
export class SalariesRepository extends Repository<SalariesEntity> {
  constructor(private dataSource: DataSource) {
    super(SalariesEntity, dataSource.createEntityManager());
  }

  async findAndCountSalaries(params): Promise<any> {
    const { search, order, skip, take } = params;
    return this.findAndCount({
      where: search,
      order,
      skip,
      take,
    });
  }
}
