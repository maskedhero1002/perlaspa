import {
  IsString,
  IsNotEmpty,
  Matches,
  IsNumber,
  IsDate,
  IsDateString,
} from 'class-validator';
import { i18nValidationMessage } from 'nestjs-i18n';
import { UniqueDecorator } from '@shared/decorators/unique.decorator';
import { Type } from 'class-transformer';

export class CreateSalariesDto {
  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  employee_id: number;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  shift: number;

  @IsDateString()
  @IsNotEmpty()
  checkinDate: Date;
}
