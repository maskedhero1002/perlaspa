import { Injectable } from '@nestjs/common';
import { Like } from 'typeorm';

import { SalariesRepository } from './salary.repository';
import { CreateSalariesDto } from './salary.dto';
import { getTemplate } from '@utils/getTemplate';
import { SalariesEntity } from '@entity';
import { SalaryShiftEnum } from '@common/enum/salary.enum';

export interface SalariesInterface extends SalariesEntity {
  action?: string;
}

export interface SalariesTableInterface {
  date: string;
  dayShift: string;
  noonShift: string;
}

@Injectable()
export class SalariesService {
  constructor(private salariesRepository: SalariesRepository) {}

  async findOneById(id: number): Promise<any> {
    return this.salariesRepository.findOne({
      where: { id },
    });
  }

  async findAllSalariesOfEmployee(query: any): Promise<any> {
    const checkinFormCompliled = getTemplate(
      'pages/employees/form/checkin.hbs',
    );

    const salaryDataTable: SalariesTableInterface[] = [];

    const now = new Date();

    const allDateInMonth = this.getDaysInMonth(
      now.getMonth(),
      now.getFullYear(),
    );

    const { draw, start, length, order, filter } = query;

    for (let i = 0; i < allDateInMonth.length; i++) {
      const dateiso = allDateInMonth[i];
      const date = new Date(dateiso);
      const marked = await this.findMarkedShiftByDate(dateiso);
      let dayChecked = false;
      let noonChecked = false;
      let shiftId;
      if (marked.length > 0) {
        marked.forEach((i) => {
          if (i.shift == SalaryShiftEnum.MORNING_SHIFT) {
            dayChecked = true;
          }
          if (i.shift == SalaryShiftEnum.AFTERNOON_SHIFT) {
            noonChecked = true;
          }
          shiftId = i.id;
        });
      }
      salaryDataTable.push({
        date: date.toLocaleDateString(),
        dayShift: checkinFormCompliled({
          index: i + '-1',
          employeeId: filter.employee_id,
          shift: SalaryShiftEnum.MORNING_SHIFT,
          checkinDate: date.toISOString(),
          checked: dayChecked ? 'checked' : '',
          shiftId,
        }),
        noonShift: checkinFormCompliled({
          index: i + '-2',
          employeeId: filter.employee_id,
          shift: SalaryShiftEnum.AFTERNOON_SHIFT,
          checkinDate: date.toISOString(),
          checked: noonChecked ? 'checked' : '',
          shiftId,
        }),
      });
    }

    const output = {
      draw: 1,
      iTotalRecords: allDateInMonth.length,
      iTotalDisplayRecords: allDateInMonth.length,
      aaData: salaryDataTable,
    };

    return output;
  }

  async createSalaries(param: CreateSalariesDto) {
    const newSalaries = this.salariesRepository.create(param);
    await this.salariesRepository.save(newSalaries);
  }

  async deleteSalary(id: number) {
    await this.salariesRepository.delete({ id });
  }

  async findMarkedShiftByDate(date: string) {
    const shifts = await this.salariesRepository
      .createQueryBuilder('salaries')
      .select('*')
      .addSelect(
        'DATE_FORMAT(`salaries`.`checkin_date`, "%d-%m-%y") as compareDate',
      )
      .where('`salaries`.`checkin_date` = :checkDate', { checkDate: date })
      .getRawMany();

    return shifts;
  }

  async exportByEmployeeId(id: number) {
    return this.salariesRepository.find({
      where: {
        employee_id: id,
      },
      relations: {
        employee: true,
      },
      select: {
        employee: {
          name: true,
          salary_range: true,
        },
      },
    });
  }

  getDaysInMonth(month, year) {
    const date = new Date(year, month, 1);
    const days = [];
    while (date.getMonth() === month) {
      const newDate = new Date(date);
      // newDate.setDate(newDate.getDate() + 1);
      days.push(newDate);
      date.setDate(date.getDate() + 1);
    }
    return days;
  }

  createFilterQueryForDatatable(filter: object) {
    if (!filter) {
      return {};
    }

    return Object.fromEntries(
      Object.keys(filter).map((key) => [key, Like(`%${filter[key]}%`)]),
    );
  }
}
