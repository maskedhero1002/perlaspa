import { Injectable } from '@nestjs/common';
import { In, Like } from 'typeorm';

import { ProductRepository } from './product.repository';
import { Products } from '@entity/Product.entity';
import { CreateProductDto, UpdateProductDto } from './Product.dto';
import { ProductGroupService } from '../productGroup/productGroup.service';
import { getTemplate } from '@utils/getTemplate';

export interface ProductInterface extends Products {
  groupName: string;
  action?: string;
}

@Injectable()
export class ProductService {
  constructor(private productRepository: ProductRepository) {}

  async findOne(id: number): Promise<any> {
    return this.productRepository.findOne({
      where: { id: id },
      relations: {
        productGroup: true,
      },
    });
  }

  async findAllProduct(query: any): Promise<any> {
    const { draw, start, length, order, filter } = query;

    const updateButtonCompliled = getTemplate('common/button/update.hbs');
    const deleteButtonCompliled = getTemplate('common/button/delete.hbs');
    const imageCompiled = getTemplate('common/image.hbs');

    const [allProducts, totalRecords] =
      await this.productRepository.findAndCountProducts({
        search: this.createFilterQueryForDatatable(filter),
        order: order,
        skip: start,
        take: length,
      });

    const paginationDataArr = allProducts.map((product: ProductInterface) => {
      product.img = imageCompiled({ imageUrl: product.img });
      const updateBtn = updateButtonCompliled({
        id: product.id,
      });
      const deleteBtn = deleteButtonCompliled({
        id: product.id,
      });

      product.action = `<div class="btn-group" role="group">${updateBtn} ${deleteBtn}</div>`;

      product.groupName = product.productGroup ? product.productGroup.name : '';
      return product;
    });

    const output = {
      draw: +draw,
      iTotalRecords: totalRecords,
      iTotalDisplayRecords: totalRecords,
      aaData: paginationDataArr,
    };

    return output;
  }

  async createProduct(param: CreateProductDto) {
    const newProduct = this.productRepository.create(param);
    await this.productRepository.save(newProduct);
  }

  async updateProduct(param: UpdateProductDto) {
    await this.productRepository.update({ id: param.id }, param);
  }

  async deleteProduct(id: number) {
    await this.productRepository.delete({ id });
  }

  createFilterQueryForDatatable(filter: object) {
    if (!filter) {
      return {};
    }

    return Object.fromEntries(
      Object.keys(filter).map((key) => {
        if (typeof filter[key] == 'object') {
          return [key, In(filter[key])];
        } else {
          return [key, Like(`%${filter[key]}%`)];
        }
      }),
    );
  }
}
