import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { ProductsEntity } from '@entity';

@Injectable()
export class ProductRepository extends Repository<ProductsEntity> {
  constructor(private dataSource: DataSource) {
    super(ProductsEntity, dataSource.createEntityManager());
  }

  async findAndCountProducts(params): Promise<any> {
    const { search, order, skip, take } = params;
    return this.findAndCount({
      where: search,
      order,
      skip,
      take,
      relations: {
        productGroup: true,
      },
      select: {
        productGroup: {
          name: true,
        },
      },
    });
  }
}
