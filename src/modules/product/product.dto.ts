import {
  IsString,
  IsNotEmpty,
  Matches,
  IsNumber,
  IsOptional,
  ValidateIf,
} from 'class-validator';
import { i18nValidationMessage } from 'nestjs-i18n';
import { UniqueDecorator } from '@shared/decorators/unique.decorator';
import { Type } from 'class-transformer';

export class CreateProductDto {
  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  name: string;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  price: number;

  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  description: string;

  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  img: string;

  // @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  // @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  // unit: string;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  duration: number;

  @IsNumber()
  @IsOptional({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  product_group_id: number;
}

export class UpdateProductDto {
  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  id: number;

  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  name: string;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  price: number;

  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  description: string;

  // @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  // @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  // unit: string;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  duration: number;

  @ValidateIf((value) => value)
  @IsNumber()
  @IsOptional({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  product_group_id: number;
}
