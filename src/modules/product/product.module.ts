import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ProductRepository } from './product.repository';
import { EmployeesEntity, ProductGroupsEntity, ProductsEntity } from '@entity';
import { productsController } from './product.controller';
import { ProductService } from './product.service';
import { ProductGroupService } from '../productGroup/productGroup.service';
import { ProductGroupRepository } from '../productGroup/productGroup.repository';
import { ProductGroupModule } from '@modules/productGroup/productGroup.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProductsEntity, ProductGroupsEntity]),
    ProductGroupModule,
  ],
  providers: [
    ProductService,
    ProductRepository,
    ProductGroupService,
    ProductGroupRepository,
  ],
  controllers: [productsController],
  exports: [
    ProductService,
    ProductRepository,
    ProductGroupService,
    ProductGroupRepository,
  ],
})
export class ProductModule {}
