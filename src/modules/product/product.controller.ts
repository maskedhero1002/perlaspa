import 'reflect-metadata';
import {
  Controller,
  UseFilters,
  UseGuards,
  Render,
  Get,
  Req,
  Post,
  Body,
  Res,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthExceptionFilter } from '@shared/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '@shared/guards/authenticated.guard';
import { DataTableQuery } from '@decorators/datatable_query.decorator';
import { AuthSuperAdminGuard } from '@shared/guards/authSuperAdmin.guard';
import { ApiFormExceptionFilter } from '@shared/filters/api.form-exceptions.filter';
import { Response } from 'express';
import { ProductService } from './product.service';
import { CreateProductDto, UpdateProductDto } from './product.dto';
import { ProductGroupService } from '../productGroup/productGroup.service';
import { ProductGroups } from '@entity/productGroup.entity';

@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
@Controller('products')
export class productsController {
  constructor(
    private readonly productService: ProductService,
    private productGroupService: ProductGroupService,
  ) {}

  @Get('/list')
  @UseGuards(AuthSuperAdminGuard)
  @Render('pages/products/index')
  async renderproductList(@Req() req) {
    return;
  }

  @Get('/views')
  @Render('pages/products/views')
  async renderproductViews(@Req() req: any) {
    let productGroupInfo: ProductGroups;
    let childs: any = '';
    if (req.query.g) {
      productGroupInfo = await this.productGroupService.findOneWithChild(
        Number(req.query.g),
      );
      childs = productGroupInfo.has_child
        ? productGroupInfo.childs.map((i) => i.id)
        : '';
    }

    const productsProcess = this.productService.findAllProduct({
      draw: 1,
      start: 0,
      length: 6,
      order: { id: 'asc' },
      filter:
        req.query.s || req.query.g
          ? {
              name: req.query.s,
              product_group_id: childs ? childs : req.query.g,
            }
          : {},
    });
    const productGroupProcess = this.productGroupService.findAllProductGroup(
      {},
    );
    const [products, productGroup] = await Promise.all([
      productsProcess,
      productGroupProcess,
    ]);
    return {
      layout: 'customer',
      isCustomer: req.user.role == 'customer',
      products,
      page: 1,
      productGroup,
    };
  }

  @Get('/views/:page')
  @Render('pages/products/views')
  async renderproductViewsPage(@Param('page') page: string, @Req() req) {
    const pageNumb = Number(page);
    const start = pageNumb * 6 - 6;
    let productGroupInfo: ProductGroups;
    let childs: any = '';

    if (req.query.g) {
      productGroupInfo = await this.productGroupService.findOneWithChild(
        Number(req.query.g),
      );
      childs = productGroupInfo.has_child
        ? productGroupInfo.childs.map((i) => i.id)
        : '';
    }

    const productsProcess = this.productService.findAllProduct({
      draw: 1,
      start: start,
      length: pageNumb * 6,
      order: { id: 'asc' },
      filter:
        req.query.s || req.query.g
          ? {
              name: req.query.s,
              product_group_id: childs ? childs : req.query.g,
            }
          : {},
    });
    const productGroupProcess = this.productGroupService.findAllProductGroup(
      {},
    );
    const [products, productGroup] = await Promise.all([
      productsProcess,
      productGroupProcess,
    ]);
    return {
      layout: 'customer',
      isCustomer: req.user.role == 'customer',
      products,
      page: pageNumb,
      productGroup,
    };
  }

  @Get('/datatable')
  @UseGuards(AuthSuperAdminGuard)
  async getAllproducts(@DataTableQuery() query) {
    return await this.productService.findAllProduct(query);
  }

  @Get('/modal-create')
  @UseGuards(AuthSuperAdminGuard)
  @Render('pages/products/popup/create')
  async getCreateModal() {
    const allProductGroups = await this.productGroupService.findAllProductGroup(
      {},
    );
    return { layout: false, allProductGroups: allProductGroups.aaData };
  }

  @Get('/single/:id')
  async getSingleDetail(@Param('id') id: string, @Res() res) {
    const products = await this.productService.findOne(Number(id));
    return res.json(JSON.stringify(products));
  }

  @Get('/modal-update/:id')
  @UseGuards(AuthSuperAdminGuard)
  @Render('pages/products/popup/edit')
  async getUpdateModal(@Param('id', ParseIntPipe) id: number) {
    const product = await this.productService.findOne(id);

    const allProductGroups = await this.productGroupService.findAllProductGroup(
      {},
    );
    return {
      layout: false,
      allProductGroups: allProductGroups.aaData,
      product,
    };
  }

  @Get('/modal-delete/:id')
  @UseGuards(AuthSuperAdminGuard)
  @Render('pages/products/popup/delete')
  getDeleteModal(@Param('id', ParseIntPipe) id: number) {
    return { layout: false, product: { id: id } };
  }

  @Post('/create')
  @UseGuards(AuthSuperAdminGuard)
  @UseFilters(ApiFormExceptionFilter)
  async createproduct(
    @Body() createproductDto: CreateProductDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.productService.createProduct(createproductDto);

      return res.json({
        status: 'success',
        message: 'Create product success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/update')
  @UseGuards(AuthSuperAdminGuard)
  @UseFilters(ApiFormExceptionFilter)
  async updateProductById(
    @Body() updateProductDto: UpdateProductDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.productService.updateProduct(updateProductDto);
      return res.json({
        status: 'success',
        message: 'Update product success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/delete/:id')
  @UseGuards(AuthSuperAdminGuard)
  @UseFilters(ApiFormExceptionFilter)
  async deleteCampaignById(
    @Param('id', ParseIntPipe) id: number,
    @Res() res: Response,
  ) {
    await this.productService.deleteProduct(id);
    return res.json({
      status: 'success',
      message: 'Delete product success',
    });
  }
}
