import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { EmployeesEntity, TransactionsEntity, ProductsEntity } from '@entity';
import { TransactionController } from './transaction.controller';
import { TransactionService } from './transaction.service';
import { TransactionsRepository } from './transaction.repository';
import { ProductModule } from '@modules/product/product.module';

@Module({
  imports: [TypeOrmModule.forFeature([TransactionsEntity]), ProductModule],
  providers: [TransactionService, TransactionsRepository],
  controllers: [TransactionController],
  exports: [TransactionService, TransactionsRepository],
})
export class TransactionModule {}
