import 'reflect-metadata';
import {
  Controller,
  UseFilters,
  UseGuards,
  Render,
  Get,
  Req,
  Post,
  Body,
  Res,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthExceptionFilter } from '@shared/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '@shared/guards/authenticated.guard';
import { DataTableQuery } from '@decorators/datatable_query.decorator';
import { AuthSuperAdminGuard } from '@shared/guards/authSuperAdmin.guard';
import { ApiFormExceptionFilter } from '@shared/filters/api.form-exceptions.filter';
import { Response } from 'express';
import { env } from '@env';
import { TransactionService } from './transaction.service';

@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
@UseGuards(AuthSuperAdminGuard)
@Controller('transactions')
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  @Get('/list')
  @Render('pages/transaction/index')
  async renderRoomList(@Req() req) {
    return;
  }

  @Get('/datatable')
  async getAllTransaction(@DataTableQuery() query) {
    return await this.transactionService.findAllTransactions(query);
  }
}
