import { Injectable } from '@nestjs/common';
import { Like } from 'typeorm';

import { TransactionsRepository } from './transaction.repository';
import { CreateTransactionDto } from './transaction.dto';
import { getTemplate } from '@utils/getTemplate';
import { TransactionsEntity } from '@entity';
import { Transactions } from '@entity/transaction.entity';
import { TransactionsStatusEnum } from '@common/enum/transaction.enum';

export interface BookedInterface extends Transactions {
  id: number;
  productName: string;
  customerName: string;
  roomName: string;
  time: string;
  date: string;
  action: string;
}

@Injectable()
export class TransactionService {
  constructor(private transactionsRepository: TransactionsRepository) {}

  async findOne(id: number): Promise<Transactions> {
    return this.transactionsRepository.findOne({
      where: { id: id },
      relations: {
        customer: true,
        room: true,
        product: true,
      },
      select: {
        customer: {
          name: true,
        },
        product: {
          name: true,
        },
        room: {
          name: true,
        },
      },
    });
  }

  async findAllTransactions(query: any): Promise<any> {
    const { draw, start, length, order, filter } = query;

    const confirmButtonCompliled = getTemplate(
      'pages/rooms/button/confirmButton.hbs',
    );
    const rejectButtonCompliled = getTemplate(
      'pages/rooms/button/rejectButton.hbs',
    );

    const [allTransactions, totalRecords] =
      await this.transactionsRepository.findAndCountTransactions({
        search: this.createFilterQueryForDatatable(filter),
        order: order,
        skip: start,
        take: length,
      });

    const paginationDataArr = allTransactions.map((booked: BookedInterface) => {
      const min = booked.min == 0 ? '00' : booked.min;

      const bookDate = new Date(booked.book_date);

      const confirmBtn = confirmButtonCompliled({
        id: booked.id,
        disabled:
          booked.status == TransactionsStatusEnum.SUBMITTED ? 'disabled' : '',
      });

      const rejectBtn = rejectButtonCompliled({
        id: booked.id,
      });

      booked.customerName = booked.customer ? booked.customer.name : 'Unknown';

      booked.productName = booked.product ? booked.product.name : 'Unknown';

      booked.roomName = booked.room ? booked.room.name : 'Unknown';

      booked.time = booked.hour + 'h' + min;

      booked.date = bookDate.toISOString().split('T')[0];

      booked.action =
        booked.status == TransactionsStatusEnum.PENDING
          ? `<div class="btn-group">${confirmBtn} ${rejectBtn} </div>`
          : `${confirmBtn}`;

      return booked as BookedInterface;
    });

    const output = {
      draw: +draw,
      iTotalRecords: totalRecords,
      iTotalDisplayRecords: totalRecords,
      aaData: paginationDataArr,
    };

    return output;
  }

  async createTransactions(param: CreateTransactionDto) {
    const newTransactions = this.transactionsRepository.create(param);
    await this.transactionsRepository.save(newTransactions);
  }

  async updateStatus(id: number, status: TransactionsStatusEnum) {
    await this.transactionsRepository.update({ id }, { status });
  }

  async deleteTransaction(id: number) {
    await this.transactionsRepository.delete({ id });
  }

  createFilterQueryForDatatable(filter: object) {
    if (!filter) {
      return {};
    }

    return Object.fromEntries(
      Object.keys(filter).map((key) => [key, Like(`%${filter[key]}%`)]),
    );
  }
}
