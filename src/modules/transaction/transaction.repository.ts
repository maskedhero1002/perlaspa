import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { TransactionsEntity } from '@entity';

@Injectable()
export class TransactionsRepository extends Repository<TransactionsEntity> {
  constructor(private dataSource: DataSource) {
    super(TransactionsEntity, dataSource.createEntityManager());
  }

  async findAndCountTransactions(params): Promise<any> {
    const { search, order, skip, take } = params;
    return this.findAndCount({
      where: search,
      order,
      skip,
      take,
      relations: {
        customer: true,
        product: true,
        room: true,
      },
      select: {
        customer: {
          name: true,
        },
        product: {
          name: true,
        },
        room: {
          name: true,
        },
      },
    });
  }
}
