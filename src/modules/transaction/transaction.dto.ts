import { Type } from 'class-transformer';
import { IsString, IsNotEmpty, IsNumber, IsDateString } from 'class-validator';
import { i18nValidationMessage } from 'nestjs-i18n';

export class CreateTransactionDto {
  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  customer_id: number;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  product_id: number;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  room_id: number;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  hour: number;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  min: number;

  @IsDateString()
  @IsNotEmpty()
  book_date: Date;
}
