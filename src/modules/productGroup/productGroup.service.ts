import { Injectable } from '@nestjs/common';
import { Like } from 'typeorm';

import { ProductGroupRepository } from './productGroup.repository';
import { ProductGroups } from '@entity/productGroup.entity';
import {
  CreateProductGroupDto,
  UpdateProductGroupDto,
} from './productGroup.dto';
import { getTemplate } from '@utils/getTemplate';

export interface ProductGroupInterface extends ProductGroups {
  id: number;
  name: string;
  parentName: string;
  action?: string;
}

@Injectable()
export class ProductGroupService {
  constructor(private productGroupRepository: ProductGroupRepository) {}

  async findOne(id: number): Promise<any> {
    return this.productGroupRepository.findOne({
      where: { id: id },
    });
  }

  async findOneWithChild(id: number): Promise<any> {
    return this.productGroupRepository.findOne({
      where: { id: id },
      relations: {
        childs: true,
      },
    });
  }

  async findAllProductGroup(query: any): Promise<any> {
    const { draw, start, length, order, filter } = query;

    const updateButtonCompliled = getTemplate('common/button/update.hbs');
    const deleteButtonCompliled = getTemplate('common/button/delete.hbs');

    const [allProductGroups, totalRecords] =
      await this.productGroupRepository.findAndCountProductGroups({
        search: this.createFilterQueryForDatatable(filter),
        order: order,
        skip: start,
        take: length,
      });

    const paginationDataArr = allProductGroups.map(
      (productGroup: ProductGroupInterface) => {
        if (productGroup.parentInfo) {
          productGroup.parentName = productGroup.parentInfo.name;
        } else {
          productGroup.parentName = '';
        }

        const updateBtn = updateButtonCompliled({
          id: productGroup.id,
        });
        const deleteBtn = deleteButtonCompliled({
          id: productGroup.id,
        });

        productGroup.action = `<div class="btn-group" role="group">${updateBtn} ${deleteBtn}</div>`;

        return productGroup as ProductGroupInterface;
      },
    );

    const output = {
      draw: +draw,
      iTotalRecords: totalRecords,
      iTotalDisplayRecords: totalRecords,
      aaData: paginationDataArr,
    };

    return output;
  }

  async createProductGroup(param: CreateProductGroupDto) {
    const newProductGroup = this.productGroupRepository.create(param);
    await this.productGroupRepository.save(newProductGroup);
  }

  async updateProductGroup(param: UpdateProductGroupDto) {
    await this.productGroupRepository.update({ id: param.id }, param);
  }

  async deleteProductGroup(id: number) {
    await this.productGroupRepository
      .createQueryBuilder('product_groups')
      .delete()
      .where('product_groups.id = :id OR  product_groups.parent = :id', { id })
      .execute();
  }

  createFilterQueryForDatatable(filter: object) {
    if (!filter) {
      return {};
    }

    return Object.fromEntries(
      Object.keys(filter).map((key) => [key, Like(`%${filter[key]}%`)]),
    );
  }
}
