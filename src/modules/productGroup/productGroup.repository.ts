import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { ProductGroupsEntity } from '@entity';

@Injectable()
export class ProductGroupRepository extends Repository<ProductGroupsEntity> {
  constructor(private dataSource: DataSource) {
    super(ProductGroupsEntity, dataSource.createEntityManager());
  }

  async findAndCountProductGroups(params): Promise<any> {
    const { search, order, skip, take } = params;
    return this.findAndCount({
      where: search,
      order,
      skip,
      take,
      relations: {
        parentInfo: true,
        childs: true,
      },
      select: {
        id: true,
        name: true,
        parentInfo: {
          name: true,
        },
      },
    });
  }
}
