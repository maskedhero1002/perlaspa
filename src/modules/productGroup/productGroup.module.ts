import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { EmployeesEntity, ProductGroupsEntity, ProductsEntity } from '@entity';
import { ProductGroupsController } from './productGroup.controller';
import { ProductGroupService } from './productGroup.service';
import { ProductGroupRepository } from './productGroup.repository';

@Module({
  imports: [TypeOrmModule.forFeature([ProductGroupsEntity])],
  providers: [ProductGroupService, ProductGroupRepository],
  controllers: [ProductGroupsController],
  exports: [ProductGroupService, ProductGroupRepository],
})
export class ProductGroupModule {}
