import 'reflect-metadata';
import {
  Controller,
  UseFilters,
  UseGuards,
  Render,
  Get,
  Req,
  Post,
  Body,
  Res,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthExceptionFilter } from '@shared/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '@shared/guards/authenticated.guard';
import { DataTableQuery } from '@decorators/datatable_query.decorator';
import { AuthSuperAdminGuard } from '@shared/guards/authSuperAdmin.guard';
import { ApiFormExceptionFilter } from '@shared/filters/api.form-exceptions.filter';
import { Response } from 'express';
import { env } from '@env';
import {
  CreateProductGroupDto,
  UpdateProductGroupDto,
} from './ProductGroup.dto';
import { ProductGroupService } from './productGroup.service';

@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
@Controller('product-group')
export class ProductGroupsController {
  constructor(private readonly productGroupService: ProductGroupService) {}

  @Get('/list')
  @Render('pages/productGroups/index')
  async renderProductGroupList(@Req() req) {
    return;
  }

  @Get('/datatable')
  async getAllProductGroups(@DataTableQuery() query) {
    return await this.productGroupService.findAllProductGroup(query);
  }

  @Get('/modal-create')
  @Render('pages/productGroups/popup/create')
  async getCreateModal() {
    const allProductGroups = await this.productGroupService.findAllProductGroup(
      {},
    );
    return {
      layout: false,
      allProductGroups: allProductGroups.aaData.filter((i) => !i.parentName),
    };
  }

  @Get('/modal-update/:id')
  @Render('pages/productGroups/popup/edit')
  async getUpdateModal(@Param('id', ParseIntPipe) id: number) {
    const productGroup = await this.productGroupService.findOne(id);
    const allProductGroups = await this.productGroupService.findAllProductGroup(
      {},
    );
    return {
      layout: false,
      productGroup,
      allProductGroups: allProductGroups.aaData,
    };
  }

  @Get('/modal-delete/:id')
  @Render('pages/productGroups/popup/delete')
  getDeleteModal(@Param('id', ParseIntPipe) id: number) {
    return { layout: false, productGroup: { id: id } };
  }

  @Post('/create')
  @UseFilters(ApiFormExceptionFilter)
  async createProductGroup(
    @Body() createProductGroupDto: CreateProductGroupDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.productGroupService.createProductGroup(createProductGroupDto);

      return res.json({
        status: 'success',
        message: 'Create ProductGroup success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/update')
  @UseFilters(ApiFormExceptionFilter)
  async updateProductById(
    @Body() updateProductGroupDto: UpdateProductGroupDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.productGroupService.updateProductGroup(updateProductGroupDto);
      return res.json({
        status: 'success',
        message: 'Update product group success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/delete/:id')
  @UseFilters(ApiFormExceptionFilter)
  async deleteCampaignById(
    @Param('id', ParseIntPipe) id: number,
    @Res() res: Response,
  ) {
    await this.productGroupService.deleteProductGroup(id);
    return res.json({
      status: 'success',
      message: 'Delete product group success',
    });
  }
}
