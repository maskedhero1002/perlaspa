import {
  IsString,
  IsNotEmpty,
  Matches,
  IsNumber,
  IsOptional,
} from 'class-validator';
import { i18nValidationMessage } from 'nestjs-i18n';
import { UniqueDecorator } from '@shared/decorators/unique.decorator';
import { Type } from 'class-transformer';

export class CreateProductGroupDto {
  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  name: string;

  @IsNumber()
  @IsOptional({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  parent: number;
}

export class UpdateProductGroupDto {
  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  id: number;

  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  name: string;

  @IsNumber()
  @IsOptional({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  parent: number;
}
