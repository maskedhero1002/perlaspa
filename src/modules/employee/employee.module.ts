import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { EmployeeRepository } from './employee.repository';
import { EmployeesEntity } from '@entity';
import { EmployeesController } from './employee.controller';
import { EmployeeService } from './employee.service';

@Module({
  imports: [TypeOrmModule.forFeature([EmployeesEntity])],
  providers: [EmployeeService, EmployeeRepository],
  controllers: [EmployeesController],
  exports: [EmployeeService],
})
export class EmployeeModule {}
