import { Injectable } from '@nestjs/common';
import { Like } from 'typeorm';

import { EmployeeRepository } from './employee.repository';
import { Employees } from '@entity/employee.entity';
import { CreateEmployeeDto, UpdateEmployeeDto } from './employee.dto';
import { getTemplate } from '@utils/getTemplate';
import { EmployeesEntity } from '@entity';

export interface EmployeeInterface extends EmployeesEntity {
  action?: string;
}

@Injectable()
export class EmployeeService {
  constructor(private employeeRepository: EmployeeRepository) {}

  async findOneByEmail(email: string): Promise<any> {
    return this.employeeRepository.findOne({
      where: { email: email },
    });
  }

  async findOneById(id: number): Promise<any> {
    return this.employeeRepository.findOne({
      where: { id },
    });
  }

  async findAllEmployee(query: any): Promise<any> {
    const { draw, start, length, order, filter } = query;

    const updateButtonCompliled = getTemplate('common/button/update.hbs');
    const deleteButtonCompliled = getTemplate('common/button/delete.hbs');
    const textLinkCombine = getTemplate('common/textLink.hbs');

    const [allEmployees, totalRecords] =
      await this.employeeRepository.findAndCountEmployees({
        search: this.createFilterQueryForDatatable(filter),
        order: order,
        skip: start,
        take: length,
      });

    const paginationDataArr = allEmployees.map(
      (employee: EmployeeInterface) => {
        employee.name = textLinkCombine({
          location: '/employees/checkin/' + employee.id,
          textListContent: employee.name,
        });
        const updateBtn = updateButtonCompliled({
          id: employee.id,
        });
        const deleteBtn = deleteButtonCompliled({
          id: employee.id,
        });

        employee.action = `<div class="btn-group" role="group">${updateBtn} ${deleteBtn}</div>`;
        return employee;
      },
    );

    const output = {
      draw: +draw,
      iTotalRecords: totalRecords,
      iTotalDisplayRecords: totalRecords,
      aaData: paginationDataArr,
    };

    return output;
  }

  async createEmployee(param: CreateEmployeeDto) {
    const newEmployee = this.employeeRepository.create(param);
    await this.employeeRepository.save(newEmployee);
  }

  async updateEmployee(param: UpdateEmployeeDto) {
    await this.employeeRepository.update({ id: param.id }, param);
  }

  async deleteEmployee(id: number) {
    await this.employeeRepository.delete({ id });
  }

  createFilterQueryForDatatable(filter: object) {
    if (!filter) {
      return {};
    }

    return Object.fromEntries(
      Object.keys(filter).map((key) => [key, Like(`%${filter[key]}%`)]),
    );
  }
}
