import 'reflect-metadata';
import {
  Controller,
  UseFilters,
  UseGuards,
  Render,
  Get,
  Req,
  Post,
  Body,
  Res,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthExceptionFilter } from '@shared/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '@shared/guards/authenticated.guard';
import { DataTableQuery } from '@decorators/datatable_query.decorator';
import { AuthSuperAdminGuard } from '@shared/guards/authSuperAdmin.guard';
import { ApiFormExceptionFilter } from '@shared/filters/api.form-exceptions.filter';
import { Response } from 'express';
import { env } from '@env';
import { EmployeeService } from './employee.service';
import { CreateEmployeeDto, UpdateEmployeeDto } from './employee.dto';

@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
@UseGuards(AuthSuperAdminGuard)
@Controller('employees')
export class EmployeesController {
  constructor(private readonly employeeService: EmployeeService) {}

  @Get('/list')
  @Render('pages/employees/index')
  async renderEmployeeList(@Req() req) {
    return;
  }

  @Get('/checkin/datatable/:id')
  async getAllEmployeesCheckin(@DataTableQuery() query) {
    return await this.employeeService.findAllEmployee(query);
  }

  @Get('/checkin/:id')
  @Render('pages/employees/checkin')
  async renderEmployeeCheckin(@Req() req, @Param('id') id: string) {
    const employee = await this.employeeService.findOneById(Number(id));
    return {
      employee,
    };
  }

  @Get('/datatable')
  async getAllEmployees(@DataTableQuery() query) {
    return await this.employeeService.findAllEmployee(query);
  }

  @Get('/modal-create')
  @Render('pages/employees/popup/create')
  getCreateModal() {
    return { layout: false };
  }

  @Get('/modal-update/:id')
  @Render('pages/employees/popup/edit')
  async getUpdateModal(@Param('id', ParseIntPipe) id: number) {
    const employee = await this.employeeService.findOneById(id);

    return {
      layout: false,
      employee,
    };
  }

  @Get('/modal-delete/:id')
  @Render('pages/employees/popup/delete')
  getDeleteModal(@Param('id', ParseIntPipe) id: number) {
    return { layout: false, employee: { id: id } };
  }

  @Post('/create')
  @UseFilters(ApiFormExceptionFilter)
  async createEmployee(
    @Body() createEmployeeDto: CreateEmployeeDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.employeeService.createEmployee(createEmployeeDto);

      return res.json({
        status: 'success',
        message: 'Create employee success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/update')
  @UseFilters(ApiFormExceptionFilter)
  async updateEmployeeById(
    @Body() updateEmployeeDto: UpdateEmployeeDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.employeeService.updateEmployee(updateEmployeeDto);
      return res.json({
        status: 'success',
        message: 'Update employee success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/delete/:id')
  @UseFilters(ApiFormExceptionFilter)
  async deleteEmployeeById(
    @Param('id', ParseIntPipe) id: number,
    @Res() res: Response,
  ) {
    await this.employeeService.deleteEmployee(id);
    return res.json({
      status: 'success',
      message: 'Delete Employee success',
    });
  }
}
