import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { EmployeesEntity } from '@entity';

@Injectable()
export class EmployeeRepository extends Repository<EmployeesEntity> {
  constructor(private dataSource: DataSource) {
    super(EmployeesEntity, dataSource.createEntityManager());
  }

  async findAndCountEmployees(params): Promise<any> {
    const { search, order, skip, take } = params;
    return this.findAndCount({
      where: search,
      order,
      skip,
      take,
    });
  }
}
