import 'reflect-metadata';
import {
  Controller,
  UseFilters,
  UseGuards,
  Render,
  Get,
  Req,
  Post,
  Body,
  Res,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthExceptionFilter } from '@shared/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '@shared/guards/authenticated.guard';
import { DataTableQuery } from '@decorators/datatable_query.decorator';
import { AuthSuperAdminGuard } from '@shared/guards/authSuperAdmin.guard';
import { ApiFormExceptionFilter } from '@shared/filters/api.form-exceptions.filter';
import { Response } from 'express';
import { env } from '@env';
import { BookRoomDto, CreateRoomDto, UpdateRoomDto } from './room.dto';
import { RoomService } from './room.service';
import { ProductService } from '@modules/product/product.service';
import { TransactionService } from '@modules/transaction/transaction.service';

@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
@Controller('rooms')
export class RoomsController {
  constructor(
    private readonly roomService: RoomService,
    private readonly productsService: ProductService,
    private readonly transactionService: TransactionService,
  ) {}

  @Get('/book-manage/datatable')
  async getAllrBooked(@DataTableQuery() query) {
    return await this.roomService.getBookedDatatable(query);
  }

  @Get('/modal-confirm/:id')
  @Render('pages/rooms/popup/confirm')
  async getConfirmModal(@Param('id', ParseIntPipe) id: number) {
    const transaction = await this.transactionService.findOne(id);
    return {
      layout: false,
      transaction,
    };
  }

  @Get('/modal-reject/:id')
  @Render('pages/rooms/popup/reject')
  async getRejectModal(@Param('id', ParseIntPipe) id: number) {
    const transaction = await this.transactionService.findOne(id);
    return {
      layout: false,
      transaction,
    };
  }

  @Get('/modal-update/:id')
  @Render('pages/rooms/popup/edit')
  async getUpdateModal(@Param('id', ParseIntPipe) id: number) {
    const room = await this.roomService.findOne(id);
    return {
      layout: false,
      room,
    };
  }

  @Get('/modal-delete/:id')
  @Render('pages/rooms/popup/delete')
  getDeleteModal(@Param('id', ParseIntPipe) id: number) {
    return { layout: false, room: { id: id } };
  }

  @Post('/confirm/:id')
  @UseFilters(ApiFormExceptionFilter)
  async confirmBooked(
    @Param('id', ParseIntPipe) id: number,
    @Res() res: Response,
  ) {
    await this.roomService.confirmBooked(id);
    return res.json({
      status: 'success',
      message: 'Xác nhận đơn đặt thành công',
    });
  }

  @Post('/reject/:id')
  @UseFilters(ApiFormExceptionFilter)
  async rejectBooked(
    @Param('id', ParseIntPipe) id: number,
    @Res() res: Response,
  ) {
    await this.roomService.rejectBooked(id);
    return res.json({
      status: 'success',
      message: 'Hủy đơn đặt thành công',
    });
  }

  @Post('/delete/:id')
  @UseFilters(ApiFormExceptionFilter)
  async deleteCampaignById(
    @Param('id', ParseIntPipe) id: number,
    @Res() res: Response,
  ) {
    await this.roomService.deleteRoom(id);
    return res.json({
      status: 'success',
      message: 'Xóa phòng thành công',
    });
  }

  @Get('/list')
  @Render('pages/rooms/index')
  async renderRoomList(@Req() req) {
    return;
  }

  @Get('/book-manage')
  @Render('pages/rooms/book-manage')
  async renderBookManage(@Req() req) {
    return;
  }

  @Get('/datatable')
  async getAllrooms(@DataTableQuery() query) {
    return await this.roomService.findAllRoom(query);
  }

  @Get('/modal-create')
  @Render('pages/rooms/popup/create')
  async getCreateModal() {
    return {
      layout: false,
    };
  }

  @Get('/modal-book')
  @Render('pages/rooms/popup/booking')
  async getBookModal(@Req() req: any) {
    req.query.min = req.query.min != '0' ? req.query.min : '00';
    const services = await this.productsService.findAllProduct({});
    return {
      layout: false,
      dataTime: req.query,
      services: services.aaData,
      roomId: req.query.room,
      customerId: req.user.id,
      bookDate: req.query.bookdate,
    };
  }

  @Post('/create')
  @UseFilters(ApiFormExceptionFilter)
  async createroom(
    @Body() createRoomDto: CreateRoomDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.roomService.createRoom(createRoomDto);

      return res.json({
        status: 'success',
        message: 'Thêm phòng thành công',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/update')
  @UseFilters(ApiFormExceptionFilter)
  async updateProductById(
    @Body() updateroomDto: UpdateRoomDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.roomService.updateRoom(updateroomDto);
      return res.json({
        status: 'success',
        message: 'Update room success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/book')
  @UseFilters(ApiFormExceptionFilter)
  async bookRoom(
    @Body() bookRoomDto: BookRoomDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      if (bookRoomDto.book_date == 'null') {
        return res.json({
          status: 'error',
          message: 'Hãy chọn ngày đặt dịch vụ trước !!!',
        });
      }
      if (!bookRoomDto.product_id) {
        return res.json({
          status: 'error',
          message: 'Hãy chọn một dịch vụ',
        });
      }
      await this.roomService.bookRoom(bookRoomDto);
      return res.json({
        status: 'success',
        message: 'Đặt phòng và dịch vụ thành công',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: 'Lỗi máy chủ',
      });
    }
  }

  @Get('/booked')
  @UseFilters(ApiFormExceptionFilter)
  async getAllBookRoom(@Res() res: Response, @Req() req: any) {
    try {
      const booking = await this.roomService.getAllRoomBookingOfDate(
        req.query.date,
      );
      return res.json({
        booking,
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }
}
