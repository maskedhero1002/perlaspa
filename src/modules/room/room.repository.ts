import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { RoomsEntity } from '@entity';

@Injectable()
export class RoomRepository extends Repository<RoomsEntity> {
  constructor(private dataSource: DataSource) {
    super(RoomsEntity, dataSource.createEntityManager());
  }

  async findAndCountRooms(params): Promise<any> {
    const { search, order, skip, take } = params;
    return this.findAndCount({
      where: search,
      order,
      skip,
      take,
    });
  }
}
