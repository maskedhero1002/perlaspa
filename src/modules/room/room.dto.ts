import {
  IsString,
  IsNotEmpty,
  Matches,
  IsNumber,
  IsOptional,
  IsDateString,
} from 'class-validator';
import { i18nValidationMessage } from 'nestjs-i18n';
import { UniqueDecorator } from '@shared/decorators/unique.decorator';
import { Type } from 'class-transformer';

export class CreateRoomDto {
  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  name: string;
}

export class UpdateRoomDto {
  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  id: number;

  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  name: string;
}

export class BookRoomDto {
  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  customer_id: number;

  product_id: number;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  room_id: number;

  @IsOptional()
  book_date: string;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  startHour: number;

  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  startMin: number;
}
