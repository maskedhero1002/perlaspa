import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { EmployeesEntity, RoomsEntity, ProductsEntity } from '@entity';
import { RoomsController } from './room.controller';
import { RoomService } from './room.service';
import { RoomRepository } from './room.repository';
import { ProductModule } from '@modules/product/product.module';
import { TransactionModule } from '@modules/transaction/transaction.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([RoomsEntity]),
    ProductModule,
    TransactionModule,
  ],
  providers: [RoomService, RoomRepository],
  controllers: [RoomsController],
  exports: [RoomService, RoomRepository],
})
export class RoomModule {}
