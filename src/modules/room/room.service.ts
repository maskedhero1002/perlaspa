import { Inject, Injectable } from '@nestjs/common';
import { Like } from 'typeorm';

import { RoomRepository } from './room.repository';
import { Rooms } from '@entity/Room.entity';
import { BookRoomDto, CreateRoomDto, UpdateRoomDto } from './room.dto';
import { getTemplate } from '@utils/getTemplate';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { TransactionService } from '@modules/transaction/transaction.service';
import { Transactions } from '@entity/transaction.entity';
import { TransactionsStatusEnum } from '@common/enum/transaction.enum';

export interface RoomInterface extends Rooms {
  id: number;
  name: string;
  action?: string;
}

@Injectable()
export class RoomService {
  constructor(
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
    private readonly roomRepository: RoomRepository,
    private readonly transactionService: TransactionService,
  ) {}

  async findOne(id: number): Promise<any> {
    return this.roomRepository.findOne({
      where: { id: id },
    });
  }

  async findAllRoom(query: any): Promise<any> {
    const { draw, start, length, order, filter } = query;

    const updateButtonCompliled = getTemplate('common/button/update.hbs');
    const deleteButtonCompliled = getTemplate('common/button/delete.hbs');

    const [allRooms, totalRecords] =
      await this.roomRepository.findAndCountRooms({
        search: this.createFilterQueryForDatatable(filter),
        order: order,
        skip: start,
        take: length,
      });

    const paginationDataArr = allRooms.map((room: RoomInterface) => {
      const updateBtn = updateButtonCompliled({
        id: room.id,
      });
      const deleteBtn = deleteButtonCompliled({
        id: room.id,
      });

      room.action = `<div class="btn-group" role="group">${updateBtn} ${deleteBtn}</div>`;

      return room as RoomInterface;
    });

    const output = {
      draw: +draw,
      iTotalRecords: totalRecords,
      iTotalDisplayRecords: totalRecords,
      aaData: paginationDataArr,
    };

    return output;
  }

  async createRoom(param: CreateRoomDto) {
    const rooms = await this.roomRepository.find({
      order: { timeStartHour: 'DESC', timeStartMin: 'DESC' },
    });
    const lastestRoom = rooms[0];
    const hour = new Date();
    hour.setHours(lastestRoom.timeStartHour);
    hour.setMinutes(lastestRoom.timeStartMin + 20);
    param['timeStartHour'] = hour.getHours();

    param['timeStartMin'] = hour.getMinutes();

    const newRoom = this.roomRepository.create(param);
    await this.roomRepository.save(newRoom);
  }

  async updateRoom(param: UpdateRoomDto) {
    await this.roomRepository.update({ id: param.id }, param);
  }

  async deleteRoom(id: number) {
    await this.roomRepository.delete({ id });
  }

  async bookRoom(param: BookRoomDto) {
    const bookDate = new Date(param.book_date);
    bookDate.setHours(bookDate.getHours() + 7);
    await this.transactionService.createTransactions({
      room_id: param.room_id,
      product_id: param.product_id,
      customer_id: param.customer_id,
      hour: param.startHour,
      min: param.startMin,
      book_date: bookDate,
    });
    const booked: string = await this.cacheManager.get(
      `${bookDate.toISOString().split('T')[0]}`,
    );
    if (!booked) {
      await this.cacheManager.set(
        `${bookDate.toISOString().split('T')[0]}`,
        JSON.stringify([
          {
            roomId: param.room_id,
            startHour: param.startHour,
            startMin: param.startMin,
            status: true,
          },
        ]),
        this.dayToMs(5),
      );
    } else {
      const arrBooked = JSON.parse(booked);
      arrBooked.push({
        roomId: param.room_id,
        startHour: param.startHour,
        startMin: param.startMin,
        status: true,
      });
      await this.cacheManager.set(
        `${bookDate.toISOString().split('T')[0]}`,
        JSON.stringify(arrBooked),
        this.dayToMs(5),
      );
    }
  }

  async getBookedDatatable(query: any) {
    const booked = await this.transactionService.findAllTransactions(query);
    return booked;
  }

  async confirmBooked(id: number) {
    await this.transactionService.updateStatus(
      id,
      TransactionsStatusEnum.SUBMITTED,
    );
  }

  async rejectBooked(id: number) {
    const transaction = await this.transactionService.findOne(id);
    const bookDate = new Date(transaction.book_date);
    bookDate.setHours(bookDate.getHours() + 7);
    const cachedBooked = await this.getAllRoomBookingOfDate(
      `${bookDate.toISOString().split('T')[0]}`,
    );
    console.log(cachedBooked);
    const bookedArr = JSON.parse(cachedBooked);
    bookedArr.forEach((i, index) => {
      if (
        i.roomId == transaction.room_id &&
        i.startHour == transaction.hour &&
        i.startMin == transaction.min
      ) {
        delete bookedArr[index];
      }
    });
    const filterd = bookedArr.filter((i) => i);
    await this.cacheManager.set(
      `${bookDate.toISOString().split('T')[0]}`,
      JSON.stringify(filterd),
      this.dayToMs(5),
    );
    await this.transactionService.deleteTransaction(id);
  }

  async getAllRoomBookingOfDate(dateString: string): Promise<string> {
    const bookDate = new Date(dateString);
    bookDate.setHours(bookDate.getHours() + 7);
    const booked: string = await this.cacheManager.get(
      `${bookDate.toISOString().split('T')[0]}`,
    );
    return booked;
  }

  dayToMs(day: number) {
    return day * 24 * 60 * 60 * 1000;
  }

  createFilterQueryForDatatable(filter: object) {
    if (!filter) {
      return {};
    }

    return Object.fromEntries(
      Object.keys(filter).map((key) => [key, Like(`%${filter[key]}%`)]),
    );
  }
}
