import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CustomerRepository } from './customer.repository';
import { CustomersEntity } from '@entity';
import { CustomersController } from './customer.controller';
import { CustomerService } from './customer.service';

@Module({
  imports: [TypeOrmModule.forFeature([CustomersEntity])],
  providers: [CustomerService, CustomerRepository],
  controllers: [CustomersController],
  exports: [CustomerService, CustomerRepository],
})
export class CustomerModule {}
