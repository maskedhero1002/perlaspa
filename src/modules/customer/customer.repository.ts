import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { CustomersEntity } from '@entity';

@Injectable()
export class CustomerRepository extends Repository<CustomersEntity> {
  constructor(private dataSource: DataSource) {
    super(CustomersEntity, dataSource.createEntityManager());
  }

  async findAndCountCustomers(params): Promise<any> {
    const { search, order, skip, take } = params;
    return this.findAndCount({
      where: search,
      order,
      skip,
      take,
    });
  }
}
