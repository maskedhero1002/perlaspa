import { Injectable } from '@nestjs/common';
import { Like } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { CustomerRepository } from './customer.repository';
import { env } from '@env';
import { getTemplate } from '@utils/getTemplate';
import { MailService } from '@common/service/mail.service';
import { Customers } from '@entity/customers.entity';
import { CreateCustomerDto, UpdateCustomerDto } from './customer.dto';

export interface CustomerInterface extends Customers {
  action?: string;
}

@Injectable()
export class CustomerService {
  constructor(private customerRepository: CustomerRepository) {}

  async findOne(email: string): Promise<any> {
    return this.customerRepository.findOne({
      where: { email: email },
    });
  }

  async findOneById(id: number): Promise<any> {
    return this.customerRepository.findOne({
      where: { id },
    });
  }

  async findAllCustomer(query: any): Promise<any> {
    const { draw, start, length, order, filter } = query;

    const [allCustomers, totalRecords] =
      await this.customerRepository.findAndCountCustomers({
        search: this.createFilterQueryForDatatable(filter),
        order: order,
        skip: start,
        take: length,
      });

    const paginationDataArr = allCustomers.map((customer) => {
      return customer;
    });

    const output = {
      draw: +draw,
      iTotalRecords: totalRecords,
      iTotalDisplayRecords: totalRecords,
      aaData: paginationDataArr,
    };

    return output;
  }

  async getCustomerDatatable(query: any): Promise<any> {
    const { draw, start, length, order, filter } = query;

    const updateButtonCompliled = getTemplate('common/button/update.hbs');
    const deleteButtonCompliled = getTemplate('common/button/delete.hbs');

    const [customers, totalRecords] =
      await this.customerRepository.findAndCountCustomers({
        search: this.createFilterQueryForDatatable(filter),
        order: order,
        skip: start,
        take: length,
      });

    // modify dataArr
    const paginationDataArray = customers.map((customer: CustomerInterface) => {
      const updateBtn = updateButtonCompliled({
        id: customer.id,
      });
      const deleteBtn = deleteButtonCompliled({
        id: customer.id,
      });

      customer.action = `<div class="btn-group" role="group">${updateBtn} ${deleteBtn}</div>`;
      return customer;
    });

    const output = {
      draw: draw,
      iTotalRecords: totalRecords,
      iTotalDisplayRecords: totalRecords,
      aaData: paginationDataArray,
    };

    return output;
  }

  async createCustomer(param: CreateCustomerDto) {
    const mailService = new MailService();
    const { email, password } = param;
    const salt = bcrypt.genSaltSync(12);
    const template = getTemplate('pages/admins/component/email.hbs');

    mailService
      .from(env.mail.email)
      .to(email)
      .subject('[admin.nftdeal.io]')
      .html(template({ urlApi: env.app.urlApi, email, password }))
      .send()
      .catch((err) => {
        console.log(err);
      });

    param.password = bcrypt.hashSync(param.password, salt);

    const newCustomer = this.customerRepository.create(param);
    return this.customerRepository.save(newCustomer);
  }

  async updateCustomer(param: UpdateCustomerDto) {
    if (!param.password) {
      await this.customerRepository.update(
        { id: param.id },
        {
          name: param.name,
          email: param.email,
          phone_number: param.phone_number,
          address: param.address,
          gender: param.gender,
        },
      );
    } else {
      const salt = bcrypt.genSaltSync(12);
      param.password = bcrypt.hashSync(param.password, salt);
      await this.customerRepository.update({ id: param.id }, param);
    }
  }

  async deleteCustomer(id: number) {
    return this.customerRepository.delete(id);
  }

  createFilterQueryForDatatable(filter: object) {
    if (!filter) {
      return {};
    }

    return Object.fromEntries(
      Object.keys(filter).map((key) => [key, Like(`%${filter[key]}%`)]),
    );
  }

  //   async createAdmin(body: CreateAdminDto) {
  //     const mailService = new MailService();
  //     const { username, password } = body;
  //     const salt = bcrypt.genSaltSync(12);
  //     const template = getTemplate('pages/admins/component/email.hbs');

  //     mailService
  //       .from(env.mail.email)
  //       .to(env.mail.email)
  //       .subject('[admin.nftdeal.io]')
  //       .html(template({ urlApi: env.app.urlApi, username, password }))
  //       .send()
  //       .catch((err) => {
  //         console.log(err);
  //       });

  //     body.password = bcrypt.hashSync(body.password, salt);

  //     const newAdmin = this.adminRepository.create(body);
  //     await this.adminRepository.save(newAdmin);
  //   }
}
