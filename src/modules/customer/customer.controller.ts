import 'reflect-metadata';
import {
  Controller,
  UseFilters,
  UseGuards,
  Render,
  Get,
  Req,
  Post,
  Body,
  Res,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthExceptionFilter } from '@shared/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '@shared/guards/authenticated.guard';
import { DataTableQuery } from '@decorators/datatable_query.decorator';
import { AuthSuperAdminGuard } from '@shared/guards/authSuperAdmin.guard';
import { ApiFormExceptionFilter } from '@shared/filters/api.form-exceptions.filter';
import { Response } from 'express';
import { env } from '@env';
import { CustomerService } from './customer.service';
import { CreateCustomerDto, UpdateCustomerDto } from './customer.dto';

@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
@UseGuards(AuthSuperAdminGuard)
@Controller('customers')
export class CustomersController {
  constructor(private readonly customerService: CustomerService) {}

  @Get('/list')
  @Render('pages/customers/index')
  async renderAdminList(@Req() req) {
    return;
  }

  @Get('/datatable')
  async getCustomerDatatable(@DataTableQuery() query) {
    return await this.customerService.getCustomerDatatable(query);
  }

  @Get('/all')
  async getAllCustomers(@DataTableQuery() query) {
    return await this.customerService.findAllCustomer(query);
  }

  @Get('/modal-create')
  @Render('pages/customers/popup/create')
  getCreateModal() {
    return { layout: false };
  }

  @Get('/modal-update/:id')
  @Render('pages/customers/popup/edit')
  async getUpdateModal(@Param('id', ParseIntPipe) id: number) {
    const customer = await this.customerService.findOneById(id);

    return {
      layout: false,
      customer,
    };
  }

  @Get('/modal-delete/:id')
  @Render('pages/customers/popup/delete')
  getDeleteModal(@Param('id', ParseIntPipe) id: number) {
    return { layout: false, customer: { id: id } };
  }

  @Post('/create')
  @UseFilters(ApiFormExceptionFilter)
  async createCustomer(
    @Body() createCustomerDto: CreateCustomerDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.customerService.createCustomer(createCustomerDto);

      return res.json({
        status: 'success',
        message: 'Create Customer success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/update')
  @UseFilters(ApiFormExceptionFilter)
  async updateCustomerById(
    @Body() updateCustomerDto: UpdateCustomerDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.customerService.updateCustomer(updateCustomerDto);
      return res.json({
        status: 'success',
        message: 'Update Customer success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/delete/:id')
  @UseFilters(ApiFormExceptionFilter)
  async deleteCustomerById(
    @Param('id', ParseIntPipe) id: number,
    @Res() res: Response,
  ) {
    await this.customerService.deleteCustomer(id);
    return res.json({
      status: 'success',
      message: 'Delete Customer success',
    });
  }
}
