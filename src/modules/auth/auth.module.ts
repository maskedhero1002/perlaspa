import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from '@modules/auth/auth.controller';
import { AuthService } from '@modules/auth/auth.service';
import { AdminModule } from '@modules/admin/admin.module';
import { LocalStrategy } from '@modules/auth/strategies/local.strategy';
import { SessionSerializer } from '@modules/auth/session.serializer';
import { CustomerModule } from '@modules/customer/customer.module';

@Module({
  imports: [AdminModule, PassportModule, CustomerModule],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, SessionSerializer],
})
export class AuthModule {}
