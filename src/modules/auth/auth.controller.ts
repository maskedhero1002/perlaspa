import {
  Controller,
  Get,
  Post,
  Res,
  Render,
  UseGuards,
  Req,
  UseFilters,
  Next,
  Body,
} from '@nestjs/common';
import { Request, Response } from 'express';
import * as bcrypt from 'bcrypt';
import { LoginGuard } from '@shared/guards/login.guard';
import { AuthExceptionFilter } from '@shared/filters/auth-exceptions.filter';
import { ApiFormExceptionFilter } from '@shared/filters/api.form-exceptions.filter';
import { CustomerRepository } from '@modules/customer/customer.repository';
import { CreateCustomerDto } from '@modules/customer/customer.dto';
import { getTemplate } from '@utils/getTemplate';

@Controller('auth')
@UseFilters(AuthExceptionFilter)
export class AuthController {
  constructor(private readonly customerRepository: CustomerRepository) {}

  @Get('/login')
  @Render('auth/singin')
  singIn(@Req() req: Request, @Res() res: Response) {
    const message: string = req.flash('loginError')[0];
    const displayError = message ? 'block' : 'none';
    const prevUrl = req.flash('prevUrl')[0] || '/index';

    res.cookie('prevUrl', prevUrl, { httpOnly: true });

    return {
      layout: 'singin',
      message,
      displayError,
    };
  }

  @UseGuards(LoginGuard)
  @Post('/login')
  login(@Req() req: any, @Res() res: Response): void {
    res.clearCookie('prevUrl');

    const user = req.user;

    if (user.role == 'admin') {
      res.redirect(req.cookies.prevUrl || '/index');
    }

    res.redirect('/');
  }

  @UseFilters(ApiFormExceptionFilter)
  @Post('/signup')
  async signup(
    @Body() body: CreateCustomerDto,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    const salt = bcrypt.genSaltSync(12);

    body.password = bcrypt.hashSync(body.password, salt);

    const newCustomer = this.customerRepository.create(body);
    await this.customerRepository.save(newCustomer);

    res.redirect('/auth/login');
  }

  @Get('/signup')
  @Render('auth/signup')
  signUp(@Req() req: Request, @Res() res: Response) {
    const message: string = req.flash('loginError')[0];
    const displayError = message ? 'block' : 'none';
    const prevUrl = req.flash('prevUrl')[0] || '/login';

    res.cookie('prevUrl', prevUrl, { httpOnly: true });

    return {
      layout: 'signup',
      message,
      displayError,
    };
  }

  @Get('/logout')
  logout(@Req() req, @Res() res: Response, @Next() next): void {
    req.logout(function (err) {
      if (err) {
        return next(err);
      }
      res.redirect('/auth/login');
    });
  }
}
