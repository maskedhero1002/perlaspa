import { Injectable } from '@nestjs/common';
import { AdminService } from '@modules/admin/admin.service';
import { AdminGetResponse } from '@shared/interfaces/admin.interface';
import * as bcrypt from 'bcrypt';
import { CustomerService } from '@modules/customer/customer.service';
import { CustomerGetResponse } from '@shared/interfaces/customer.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly adminService: AdminService,
    private readonly customerService: CustomerService,
  ) {}

  async validateUser(username, password): Promise<any> {
    if (this.validateEmail(username)) {
      const customer = await this.customerService.findOne(username);

      if (!customer) {
        return null;
      }

      const isPasswordMatching = await bcrypt.compare(
        password,
        customer?.password,
      );

      customer.role = 'customer';

      if (isPasswordMatching) {
        return customer as CustomerGetResponse;
      }
    } else {
      const admin = await this.adminService.findOne(username);

      if (!admin) {
        return null;
      }

      const isPasswordMatching = await bcrypt.compare(
        password,
        admin?.password,
      );

      admin.role = 'admin';

      if (isPasswordMatching) {
        return admin as AdminGetResponse;
      }
    }

    return null;
  }

  validateEmail = (email) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      );
  };
}
