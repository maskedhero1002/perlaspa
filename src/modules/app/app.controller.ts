import {
  Get,
  Controller,
  Render,
  Req,
  Res,
  UseGuards,
  Next,
  UseFilters,
  Param,
} from '@nestjs/common';
import { Response, Request } from 'express';
import { AppService } from './app.service';
import { AuthenticatedGuard } from '@shared/guards/authenticated.guard';
import { AuthExceptionFilter } from '@shared/filters/auth-exceptions.filter';
import { I18n, I18nContext } from 'nestjs-i18n';
import { ProductGroups } from '@entity/productGroup.entity';
import { ProductService } from '@modules/product/product.service';
import { ProductGroupService } from '@modules/productGroup/productGroup.service';
import { RoomService } from '@modules/room/room.service';
import { AuthSuperAdminGuard } from '@shared/guards/authSuperAdmin.guard';

@Controller()
export class AppController {
  constructor(
    private appService: AppService,
    private readonly roomService: RoomService,
    private readonly productService: ProductService,
    private readonly productGroupService: ProductGroupService,
  ) {}

  @Get('/')
  @Render('pages/customers/home')
  entry(@Req() req: any, @Res() res: Response) {
    return {
      layout: 'customer',
      isCustomer: true,
      user: req.user,
    };
  }

  @Get('/index')
  @UseFilters(AuthExceptionFilter)
  @UseGuards(AuthenticatedGuard)
  @UseGuards(AuthSuperAdminGuard)
  @Render('home')
  getHome(@Req() req) {
    return { user: req.user, isCustomer: req.user.role == 'customer' };
  }

  @Get('/detail/:id')
  @Render('pages/products/detail')
  async renderproductDetail(@Param('id') id: string, @Req() req) {
    const products = await this.productService.findOne(Number(id));
    return {
      layout: 'customer',
      isCustomer: true,
      products,
      user: req.user,
    };
  }

  @Get('/views/:page')
  @Render('pages/products/views')
  async renderproductViewsPage(@Param('page') page: string, @Req() req) {
    const pageNumb = Number(page);
    const start = pageNumb * 6 - 6;
    let productGroupInfo: ProductGroups;
    let childs: any = '';

    if (req.query.g) {
      productGroupInfo = await this.productGroupService.findOneWithChild(
        Number(req.query.g),
      );
      childs = productGroupInfo.has_child
        ? productGroupInfo.childs.map((i) => i.id)
        : '';
    }

    const productsProcess = this.productService.findAllProduct({
      draw: 1,
      start: start,
      length: pageNumb * 6,
      order: { id: 'asc' },
      filter:
        req.query.s || req.query.g
          ? {
              name: req.query.s,
              product_group_id: childs ? childs : req.query.g,
            }
          : {},
    });
    const productGroupProcess = this.productGroupService.findAllProductGroup(
      {},
    );
    const [products, productGroup] = await Promise.all([
      productsProcess,
      productGroupProcess,
    ]);
    return {
      layout: 'customer',
      isCustomer: true,
      products,
      page: pageNumb,
      productGroup,
      user: req.user,
    };
  }

  @Get('/booking')
  @UseFilters(AuthExceptionFilter)
  @UseGuards(AuthenticatedGuard)
  @Render('pages/rooms/booking')
  async renderRoomBooking(@Req() req) {
    const rooms = await this.roomService.findAllRoom({});
    return {
      layout: 'customer',
      isCustomer: true,
      rooms: rooms.aaData,
    };
  }

  @Get('/logout')
  logout(@Req() req, @Res() res: Response, @Next() next): void {
    req.logout(function (err) {
      if (err) {
        return next(err);
      }
      res.redirect('/');
    });
  }
}
