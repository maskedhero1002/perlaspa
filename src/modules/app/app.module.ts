import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import databaseConfig from '@databases/config/index';

import { AuthModule } from '@modules/auth/auth.module';
import * as path from 'path';
import {
  AcceptLanguageResolver,
  CookieResolver,
  I18nModule,
} from 'nestjs-i18n';
import { ErrorsModule } from '@modules/errors/errors.module';
import { UploadModule } from '@modules/upload/upload.module';
import { LoggerModule } from '@shared/logger/logger.module';
import { AdminModule } from '@modules/admin/admin.module';
import { CustomerModule } from '@modules/customer/customer.module';
import { EmployeeModule } from '@modules/employee/employee.module';
import { ProductModule } from '@modules/product/product.module';
import { ProductGroupModule } from '@modules/productGroup/productGroup.module';
import { RoomModule } from '@modules/room/room.module';
import { SalariesModule } from '@modules/salary/salary.module';
import { CacheModule } from '@nestjs/cache-manager';

const options = databaseConfig as TypeOrmModuleOptions;

@Module({
  imports: [
    LoggerModule,
    AdminModule,
    UploadModule,
    ErrorsModule,
    CustomerModule,
    EmployeeModule,
    ProductModule,
    ProductGroupModule,
    RoomModule,
    AuthModule,
    SalariesModule,
    CacheModule.register({ isGlobal: true }),
    TypeOrmModule.forRoot({
      ...options,
      autoLoadEntities: true,
    }),
    I18nModule.forRoot({
      fallbackLanguage: 'en',
      loaderOptions: {
        path: path.join(__dirname, '../../', '/i18n/'),
        watch: true,
      },
      viewEngine: 'hbs',
      resolvers: [new CookieResolver(), AcceptLanguageResolver],
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
