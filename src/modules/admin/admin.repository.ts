import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { AdminEntity } from '@entity';

@Injectable()
export class AdminRepository extends Repository<AdminEntity> {
  constructor(private dataSource: DataSource) {
    super(AdminEntity, dataSource.createEntityManager());
  }

  async findAndCountAdmins(params): Promise<any> {
    const { search, order, skip, take } = params;
    return this.findAndCount({
      where: search,
      order,
      skip,
      take,
      select: {
        id: true,
        username: true,
        createdAt: true,
      },
    });
  }
}
