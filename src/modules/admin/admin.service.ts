import { Injectable } from '@nestjs/common';
import { Like } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { AdminRepository } from './admin.repository';
import { CreateAdminDto, UpdateAdminDto } from './admin.dto';
import { env } from '@env';
import { getTemplate } from '@utils/getTemplate';
import { MailService } from '@common/service/mail.service';
import { Admins } from '@entity/admin.entity';
import e from 'connect-flash';

export interface AdminInterface extends Admins {
  action?: string;
  created: string;
}

@Injectable()
export class AdminService {
  constructor(private adminRepository: AdminRepository) {}

  async findOne(username: string): Promise<any> {
    return this.adminRepository.findOne({
      where: { username: username },
    });
  }

  async findOneById(id: number): Promise<any> {
    return this.adminRepository.findOne({
      where: { id },
    });
  }

  async findAllAdmin(query: any): Promise<any> {
    const { draw, start, length, order, filter } = query;

    const updateButtonCompliled = getTemplate('common/button/update.hbs');
    const deleteButtonCompliled = getTemplate('common/button/delete.hbs');

    const [allAdmins, totalRecords] =
      await this.adminRepository.findAndCountAdmins({
        search: this.createFilterQueryForDatatable(filter),
        order: order,
        skip: start,
        take: length,
      });

    const paginationDataArr = allAdmins.map((admin: AdminInterface) => {
      const updateBtn = updateButtonCompliled({
        id: admin.id,
      });
      const deleteBtn = deleteButtonCompliled({
        id: admin.id,
      });

      admin.action = `<div class="btn-group" role="group">${updateBtn} ${deleteBtn}</div>`;
      admin.created = new Date(admin.createdAt).toLocaleString().split(',')[0];
      return admin;
    });

    const output = {
      draw: +draw,
      iTotalRecords: totalRecords,
      iTotalDisplayRecords: totalRecords,
      aaData: paginationDataArr,
    };

    return output;
  }

  createFilterQueryForDatatable(filter: object) {
    if (!filter) {
      return {};
    }

    return Object.fromEntries(
      Object.keys(filter).map((key) => [key, Like(`%${filter[key]}%`)]),
    );
  }

  async createAdmin(body: CreateAdminDto) {
    // const mailService = new MailService();
    // const { username, password } = body;
    const salt = bcrypt.genSaltSync(12);
    // const template = getTemplate('pages/admins/component/email.hbs');

    // mailService
    //   .from(env.mail.email)
    //   .to(env.mail.email)
    //   .subject('[admin.nftdeal.io]')
    //   .html(template({ urlApi: env.app.urlApi, username, password }))
    //   .send()
    //   .catch((err) => {
    //     console.log(err);
    //   });

    body.password = bcrypt.hashSync(body.password, salt);

    const newAdmin = this.adminRepository.create(body);
    await this.adminRepository.save(newAdmin);
  }

  async updateAdmin(param: UpdateAdminDto) {
    if (!param.password) {
      await this.adminRepository.update(
        { id: param.id },
        { username: param.username },
      );
    } else {
      const salt = bcrypt.genSaltSync(12);
      param.password = bcrypt.hashSync(param.password, salt);
      await this.adminRepository.update({ id: param.id }, param);
    }
  }

  async deleteAdmin(id: number, userId: number) {
    if (id == userId) {
      throw new Error('Can not delete yourself');
    } else {
      await this.adminRepository.delete({ id });
    }
  }
}
