import 'reflect-metadata';
import {
  Controller,
  UseFilters,
  UseGuards,
  Render,
  Get,
  Req,
  Post,
  Body,
  Res,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthExceptionFilter } from '@shared/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '@shared/guards/authenticated.guard';
import { AdminService } from './admin.service';
import { DataTableQuery } from '@decorators/datatable_query.decorator';
import { AuthSuperAdminGuard } from '@shared/guards/authSuperAdmin.guard';
import { ApiFormExceptionFilter } from '@shared/filters/api.form-exceptions.filter';
import { Response } from 'express';
import { CreateAdminDto, UpdateAdminDto } from './admin.dto';
import { env } from '@env';

@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
@UseGuards(AuthSuperAdminGuard)
@Controller('admins')
export class AdminsController {
  constructor(private readonly adminService: AdminService) {}

  @Get('/list')
  @Render('pages/admins/index')
  async renderAdminList(@Req() req) {
    return { isSuperAdmin: req.user.isSuperAdmin };
  }

  @Get('/all')
  async getAllAdmins(@DataTableQuery() query) {
    return await this.adminService.findAllAdmin(query);
  }

  @Get('/modal-create')
  @Render('pages/admins/popup/create')
  getCreateModal() {
    return { layout: false };
  }

  @Get('/modal-update/:id')
  @Render('pages/admins/popup/edit')
  async getUpdateModal(@Param('id', ParseIntPipe) id: number) {
    const admin = await this.adminService.findOneById(id);

    return {
      layout: false,
      admin,
    };
  }

  @Get('/modal-delete/:id')
  @Render('pages/admins/popup/delete')
  getDeleteModal(@Param('id', ParseIntPipe) id: number) {
    return { layout: false, admin: { id: id } };
  }

  @Post('/create')
  @UseFilters(ApiFormExceptionFilter)
  async createAdmin(@Body() body: CreateAdminDto, @Res() res: Response) {
    try {
      await this.adminService.createAdmin(body);
      return res.json({
        status: 'success',
        message: 'create success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/update')
  @UseFilters(ApiFormExceptionFilter)
  async updateAdminById(
    @Body() updateAdminDto: UpdateAdminDto,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.adminService.updateAdmin(updateAdminDto);
      return res.json({
        status: 'success',
        message: 'Update Admin success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }

  @Post('/delete/:id')
  @UseFilters(ApiFormExceptionFilter)
  async deleteEmployeeById(
    @Param('id', ParseIntPipe) id: number,
    @Res() res: Response,
    @Req() req: any,
  ) {
    try {
      await this.adminService.deleteAdmin(id, req.user.id);
      return res.json({
        status: 'success',
        message: 'Delete Admin success',
      });
    } catch (error) {
      return res.json({
        status: 'error',
        message: error.message,
      });
    }
  }
}
