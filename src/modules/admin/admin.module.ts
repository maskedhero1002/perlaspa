import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AdminService } from '@modules/admin/admin.service';
import { AdminRepository } from './admin.repository';
import { AdminEntity } from '@entity';
import { AdminsController } from './admin.controller';

@Module({
  imports: [TypeOrmModule.forFeature([AdminEntity])],
  providers: [AdminService, AdminRepository],
  controllers: [AdminsController],
  exports: [AdminService],
})
export class AdminModule {}
