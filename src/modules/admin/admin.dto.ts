import {
  IsString,
  IsNotEmpty,
  Matches,
  IsNumber,
  IsOptional,
  ValidateIf,
} from 'class-validator';
import { i18nValidationMessage } from 'nestjs-i18n';
import { UniqueDecorator } from '@shared/decorators/unique.decorator';
import { Admins } from '@entity/admin.entity';
import { Type } from 'class-transformer';
import { IsNull } from 'typeorm';

export class CreateAdminDto {
  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @UniqueDecorator(Admins, 'username', {
    message: i18nValidationMessage('validation.IS_UNIQUE'),
  })
  @Matches(/^[a-zA-Z0-9]{6,}$/, {
    message: i18nValidationMessage('validation.NOT_MATCH_USERNAME'),
  })
  username: string;

  @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,}$/, {
    message: i18nValidationMessage('validation.NOT_MATCH_PASSWORD'),
  })
  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  password: string;
}

export class UpdateAdminDto {
  @IsNumber()
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @Type(() => Number)
  id: number;

  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsNotEmpty({ message: i18nValidationMessage('validation.NOT_EMPTY') })
  @UniqueDecorator(Admins, 'username', {
    message: i18nValidationMessage('validation.IS_UNIQUE'),
  })
  @Matches(/^[a-zA-Z0-9]{6,}$/, {
    message: i18nValidationMessage('validation.NOT_MATCH_USERNAME'),
  })
  username: string;

  @ValidateIf((object, value) => value !== '')
  @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,}$/, {
    message: i18nValidationMessage('validation.NOT_MATCH_PASSWORD'),
  })
  @IsString({ message: i18nValidationMessage('validation.IS_STRING') })
  @IsOptional()
  password: string;
}
