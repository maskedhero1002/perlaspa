import { ethers } from 'ethers';
import { ChainId } from '@utils/rpc';

export const ContractEvents = {
  currency: {
    event: ['NewCurrency(address,string)'],
    abi: ['event NewCurrency(address currency, string imgUrl)'],
  },
};

export const ContractInfo = {
  [ChainId.BSC_TESTNET]: {
    currency: {
      address: '0xCD10873a691F1b6115D81675239be83A64116432',
      hash: '0xce878b6ba6bca6f8c04ac571b24b2cfe32fa453e61ff66c258c917ff94644259',
    },
    padi721: {
      address: '0x3507d3c618f5d8F5722dDb6fEf8737BE4cDbd6D7',
      hash: '0x639dc34caee2227d9a0321197cafc6fa7fe8cbacf55a2dca1d96c914a6fb7fa5',
    },
    padi1155: {
      address: '0x0fc139434883925Ed64130f039D8F69545Df11e9',
      hash: '0xc6da667c062f05815c5c575e35db497dcb693f8a5add89f2210d97ec252f7328',
    },
  },

  [ChainId.BSC_MAINNET]: {
    padi721: {
      address: '0x8588b02229001eef3094e2c83dceaee163d53051',
      hash: '0x5d30615e4e8a1d0c628d5d00cd3e3fa9be9069a002b40c30ec8cf760e99bae76',
      arguments: [],
    },
    padi1155: {
      address: '0x8c443612f209e87ef6a4ae9c5c11757abec182b6',
      hash: '0xcd9f412d81dd7dee106dbf6cac8b58834032984e0124328a3b97e9023eae8c74',
      arguments: [],
    },
    currency: {
      address: '0x04e93be223e61161f2e27da37c4146f0d074d69c',
      hash: '0x6c546ea164a0aa21f1ff5b2c4aaede9fcc54bf0afeab72496f2da8f5f7b9b3c6',
      arguments: [],
    },
  },
};

class ContractUtils {
  static validateAddress(address: string): string {
    if (!ethers.utils.isAddress(address)) {
      throw new Error('Invalid address');
    }
    return address;
  }
}

export default ContractUtils;
