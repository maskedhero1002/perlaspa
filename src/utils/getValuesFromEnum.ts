/**
 * Function get number values from enum (format string-number for use in migration)
 */
export const getValuesNumbericEnum = (enumParam: object) => {
  return Object.keys(enumParam).filter((key) => !isNaN(Number(key)));
};

export const getKeysNumbericEnum = (enumParam: object) => {
  return Object.keys(enumParam).filter((key) => isNaN(Number(key)));
};
