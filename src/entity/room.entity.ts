import { ProductUnitsEnum } from '@common/enum/product.enum';
import { UserGendersEnum } from '@common/enum/user.enum';
import 'reflect-metadata';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { Transactions } from './transaction.entity';

@Entity('rooms')
export class Rooms {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ name: 'time_start_hour' })
  timeStartHour: number;

  @Column({ name: 'time_start_min' })
  timeStartMin: number;

  @OneToMany((type) => Transactions, (transaction) => transaction.room)
  @JoinColumn({ name: 'id' })
  transactions: Transactions[];

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;
}
