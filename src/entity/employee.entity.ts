import { UserGendersEnum } from '@common/enum/user.enum';
import 'reflect-metadata';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { Salaries } from './salary.entity';

@Entity('employees')
export class Employees {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  phone_number: string;

  @Column({
    enum: UserGendersEnum,
  })
  gender: string;

  @Column()
  address: string;

  @Column()
  salary_range: number;

  @OneToMany((type) => Salaries, (salary) => salary.employee)
  @JoinColumn({ name: 'id' })
  salaries: Salaries[];

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;
}
