import { ProductUnitsEnum } from '@common/enum/product.enum';
import { TransactionsStatusEnum } from '@common/enum/transaction.enum';
import { UserGendersEnum } from '@common/enum/user.enum';
import 'reflect-metadata';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Customers } from './customers.entity';
import { Products } from './product.entity';
import { Rooms } from './room.entity';

@Entity('transactions')
export class Transactions {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  customer_id: number;

  @Column()
  product_id: number;

  @Column()
  room_id: number;

  @Column()
  hour: number;

  @Column()
  min: number;

  @Column()
  book_date: Date;

  @Column({
    enum: TransactionsStatusEnum,
    default: TransactionsStatusEnum.PENDING,
  })
  status: string;

  @ManyToOne((type) => Customers, (customer) => customer.transactions)
  @JoinColumn({ name: 'customer_id' })
  customer: Customers;

  @ManyToOne((type) => Products, (product) => product.transactions)
  @JoinColumn({ name: 'product_id' })
  product: Products;

  @ManyToOne((type) => Rooms, (room) => room.transactions)
  @JoinColumn({ name: 'room_id' })
  room: Rooms;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;
}
