import { ProductUnitsEnum } from '@common/enum/product.enum';
import { UserGendersEnum } from '@common/enum/user.enum';
import 'reflect-metadata';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { ProductGroups } from './productGroup.entity';
import { Transactions } from './transaction.entity';

@Entity('products')
export class Products {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  img: string;

  @Column()
  price: number;

  // @Column({ enum: ProductUnitsEnum })
  // unit: string;

  @Column()
  duration: number;

  @Column()
  product_group_id: number;

  @ManyToOne((type) => ProductGroups, (productGroup) => productGroup.products)
  @JoinColumn({ name: 'product_group_id' })
  productGroup: ProductGroups;

  @OneToMany((type) => Transactions, (transaction) => transaction.product)
  @JoinColumn({ name: 'id' })
  transactions: Transactions[];

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;
}
