import { UserGendersEnum } from '@common/enum/user.enum';
import 'reflect-metadata';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { Transactions } from './transaction.entity';

@Entity()
export class Customers {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  phone_number: string;

  @Column({
    enum: UserGendersEnum,
  })
  gender: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  address: string;

  @OneToMany((type) => Transactions, (transaction) => transaction.customer)
  @JoinColumn({ name: 'id' })
  transactions: Transactions[];

  @Column({
    name: 'created_at',
  })
  createdAt: Date;

  @Column({
    name: 'updated_at',
  })
  updatedAt: Date;
}
