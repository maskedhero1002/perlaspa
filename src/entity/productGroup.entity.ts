import { ProductUnitsEnum } from '@common/enum/product.enum';
import { UserGendersEnum } from '@common/enum/user.enum';
import 'reflect-metadata';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { Products } from './product.entity';

@Entity('product_groups')
export class ProductGroups {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  parent: number;

  @OneToMany((type) => ProductGroups, (productGroup) => productGroup.parentInfo)
  @JoinColumn({ name: 'id' })
  childs: ProductGroups[];

  @ManyToOne((type) => ProductGroups, (productGroup) => productGroup.childs)
  @JoinColumn({ name: 'parent' })
  parentInfo: ProductGroups;

  @OneToMany((type) => Products, (product) => product.productGroup)
  @JoinColumn({ name: 'id' })
  products: Products[];

  @Column()
  has_child: boolean;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;
}
