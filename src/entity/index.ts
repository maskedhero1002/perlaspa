export { Customers as CustomersEntity } from './customers.entity';
export { Admins as AdminEntity } from './admin.entity';
export { Employees as EmployeesEntity } from './employee.entity';
export { Products as ProductsEntity } from './product.entity';
export { ProductGroups as ProductGroupsEntity } from './productGroup.entity';
export { Rooms as RoomsEntity } from './room.entity';
export { Salaries as SalariesEntity } from './salary.entity';
export { Transactions as TransactionsEntity } from './transaction.entity';
