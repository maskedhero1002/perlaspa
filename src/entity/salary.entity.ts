import { ProductUnitsEnum } from '@common/enum/product.enum';
import { SalaryShiftEnum } from '@common/enum/salary.enum';
import { UserGendersEnum } from '@common/enum/user.enum';
import 'reflect-metadata';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Employees } from './employee.entity';

@Entity('salaries')
export class Salaries {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  employee_id: number;

  @Column({ enum: SalaryShiftEnum })
  shift: number;

  @Column({ name: 'checkin_date' })
  checkinDate: Date;

  @ManyToOne((type) => Employees, (employee) => employee.salaries)
  @JoinColumn({ name: 'employee_id' })
  employee: Employees;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;
}
