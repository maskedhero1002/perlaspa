export interface CustomerModel {
  customerId: number;
  email: string;
  password: string;
}

export type CustomerGetResponse = Omit<
  CustomerModel,
  'customerId' | 'password'
>;
