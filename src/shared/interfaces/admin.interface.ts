export interface AdminModel {
  adminId: number;
  username: string;
  password: string;
}

export type AdminGetResponse = Omit<AdminModel, 'adminId' | 'password'>;
