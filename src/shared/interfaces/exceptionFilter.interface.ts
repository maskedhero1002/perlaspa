export interface ExceptionFilterInterface {
  message: any;
  trace?: string;
  context?: string;
}
