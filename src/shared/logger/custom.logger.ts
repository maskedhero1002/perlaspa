const fs = require('fs');

import { Injectable, Logger, Scope } from '@nestjs/common';
import { env } from '@env';

import { ExceptionFilterInterface } from '@shared/interfaces/exceptionFilter.interface';

@Injectable({ scope: Scope.TRANSIENT })
export class LoggerService extends Logger {
  error({ message, trace, context }: ExceptionFilterInterface) {
    try {
      const messageFormat = this.formatMessage(message);
      const path = this.getPath();

      this.writeLogToFile(messageFormat, path);

      super.error(message, trace, context);

      return true;
    } catch (err) {
      return false;
    }
  }

  writeLogToFile(messageFormat, path) {
    try {
      if (!fs.existsSync(env.loggerPath)) {
        fs.mkdirSync(env.loggerPath);
      }

      fs.appendFile(path, messageFormat, (err) => {
        if (err) {
          return console.error(err);
        }
      });

      return true;
    } catch (err) {
      return false;
    }
  }

  getPath() {
    return (
      env.loggerPath + '/' + new Date().toISOString().slice(0, 10) + '.txt'
    );
  }

  formatMessage(message: any) {
    const time = new Date();

    return (
      time.toISOString().split('T')[0] +
      ' ,' +
      time.toLocaleString().split(',')[1] +
      ' [ ERROR ] ' +
      message.toString() +
      '\n'
    );
  }

  warn(message: any, context?: string) {
    // TO DO
    super.warn(message, context);
  }

  log(message: any, context?: string) {
    // TO DO
    super.log(message, context);
  }

  debug(message: any, context?: string) {
    // TO DO
    super.debug(message, context);
  }

  verbose(message: any, context?: string) {
    // TO DO
    super.verbose(message, context);
  }
}
