import { ExecutionContext, CanActivate, Injectable } from '@nestjs/common';

@Injectable()
export class AuthSuperAdminGuard implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    if (request.user) {
      return request.user.role == 'admin';
    } else {
      return false;
    }
  }
}
