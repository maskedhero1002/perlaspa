import { DataSource } from 'typeorm';
import { AppDataSourceOption } from '@databases/data-source';
import {
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
} from 'class-validator';

export function UniqueDecorator(
  entity: any,
  key: string,
  validationOptions?: ValidationOptions,
) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [entity, key],
      validator: {
        async validate(value: any, args: ValidationArguments) {
          const [entity, tableField] = args.constraints;

          if (!value) {
            return true;
          }

          const AppDataSource = new DataSource(AppDataSourceOption);

          const connection = await AppDataSource.initialize();

          const repository = connection.getRepository(entity);

          if (repository !== undefined) {
            const whereClause = {};

            whereClause[tableField] = value;

            return repository
              .findOne({
                where: whereClause,
              })
              .then((user) => {
                connection.destroy();
                if (!user) return true;
                return false;
              });
          } else {
            return false;
          }
        },
        defaultMessage(): string {
          return 'Value ' + propertyName + ' is already exist';
        },
      },
    });
  };
}
