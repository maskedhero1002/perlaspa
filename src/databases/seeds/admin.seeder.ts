import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { AdminEntity } from '../../entity/index';
import { wrapValueWithDatetime } from '../utils/wrapValuesWithDatetime';

export default class CreateAdmin implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const valuesWithDatetime = await wrapValueWithDatetime([
      {
        id: 1,
        username: 'john',
        password:
          '$2a$10$QluPhq1CI46JkrnSi0/ep.p/QzOzbYjMLSIu2prTG76cJzzgTWcUC', //changeme
      },
      {
        id: 2,
        username: 'chris',
        password:
          '$2a$10$.icLp2RkXx6oPeolsKIO5uTZGHpQKYP9EsJ56cMf0Zj3z3YvORMnq', //secret
      },
      {
        id: 3,
        username: 'maria',
        password:
          '$2a$10$XkqPyfemAD6zinXiM082iu8m1VzUzQR.JrcSzfF0wXKnJAScrEXsG', //guess
      },
    ]);
    await connection
      .createQueryBuilder()
      .insert()
      .into(AdminEntity)
      .values(valuesWithDatetime)
      .execute();
  }
}
