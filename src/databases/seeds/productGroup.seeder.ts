import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { ProductGroupsEntity } from '../../entity';
import { wrapValueWithDatetime } from '../utils/wrapValuesWithDatetime';

export default class CreateProductGroups implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const valuesWithDatetime: ProductGroupsEntity[] =
      await wrapValueWithDatetime([
        {
          id: 1,
          name: 'Sản phẩm',
          has_child: true,
        },
        {
          id: 2,
          name: 'Dịch vụ',
          has_child: '',
        },
        {
          id: 3,
          name: 'Chăm sóc da',
          parent: 1,
          has_child: '',
        },
        {
          id: 4,
          name: 'Chăm sóc tóc',
          parent: 1,
          has_child: '',
        },
      ]);
    await connection
      .createQueryBuilder()
      .insert()
      .into(ProductGroupsEntity)
      .values(valuesWithDatetime)
      .execute();
  }
}
