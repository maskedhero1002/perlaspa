import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { EmployeesEntity } from '../../entity';
import { wrapValueWithDatetime } from '../utils/wrapValuesWithDatetime';
import { UserGendersEnum } from '@common/enum/user.enum';

export default class CreateEmployees implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const valuesWithDatetime: EmployeesEntity[] = await wrapValueWithDatetime([
      {
        id: 1,
        name: 'Harper Collins',
        phone_number: '0935882169',
        gender: UserGendersEnum.MALE,
        email: 'harpercollins3234@gmail.com',
        address: '27 Le Loi Street, Ward 4, Go Vap District, Ho Chi Minh City',
        salary_range: 4000000,
      },
      {
        id: 2,
        name: 'Pearl Barnes',
        phone_number: '0838654934',
        gender: UserGendersEnum.MALE,
        email: 'pearlbarnes@gmail.com',
        address:
          '89/30 Trinh Dinh Trong Street, Phu Trung Ward, Ho Chi Minh City',
        salary_range: 5000000,
      },
      {
        id: 3,
        name: 'Rosemary Cunningham',
        phone_number: '0650732599',
        gender: UserGendersEnum.FEMALE,
        email: 'rosemarygoose3433@gmail.com',
        address: ' 1 Tan Hung St., Ward 12, Dist. 5, Ho Chi Minh City',
        salary_range: 6000000,
      },
    ]);
    await connection
      .createQueryBuilder()
      .insert()
      .into(EmployeesEntity)
      .values(valuesWithDatetime)
      .execute();
  }
}
