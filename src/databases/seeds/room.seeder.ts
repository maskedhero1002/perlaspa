import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { RoomsEntity } from '../../entity';
import { wrapValueWithDatetime } from '../utils/wrapValuesWithDatetime';

export default class CreateRooms implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const valuesWithDatetime: RoomsEntity[] = await wrapValueWithDatetime([
      {
        id: 1,
        name: 'Room 1',
        timeStartHour: 7,
        timeStartMin: 0,
      },
      {
        id: 2,
        name: 'Room 2',
        timeStartHour: 7,
        timeStartMin: 20,
      },
      {
        id: 3,
        name: 'Room 3',
        timeStartHour: 7,
        timeStartMin: 40,
      },
    ]);
    await connection
      .createQueryBuilder()
      .insert()
      .into(RoomsEntity)
      .values(valuesWithDatetime)
      .execute();
  }
}
