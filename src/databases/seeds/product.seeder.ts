import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { ProductsEntity } from '../../entity';
import { wrapValueWithDatetime } from '../utils/wrapValuesWithDatetime';
import { UserGendersEnum } from '@common/enum/user.enum';
import { ProductUnitsEnum } from '@common/enum/product.enum';

export default class CreateProducts implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const valuesWithDatetime: ProductsEntity[] = await wrapValueWithDatetime([
      {
        name: 'Cấp ẩm phục hồi GlowSkin',
        description: `Gói chăm sóc da cấp ẩm phục hồi của chúng tôi sẽ đem lại cho bạn làn da mềm mịn, đầy sức sống. Perla sử dụng các sản phẩm chất lượng cao và kỹ thuật chăm sóc chuyên nghiệp, liệu pháp này giúp khôi phục độ ẩm tự nhiên và làm dịu da, mang lại cho bạn một làn da mềm mịn và tươi trẻ hơn.
Bước 1. Ngâm chân, matxa chân thư giãn
Bước 2. Tẩy trang loại bỏ bụi bần, dầu thừa và lớp trang điểm
Bước 3. Rửa mặt giúp da sáng khoẻ sạch sâu
Bước 4. Tẩy da chết loại bỏ lớp sừng trên da, giúp da sáng
Bước 5. Xông hơi thư giãn, giúp giãn nở lỗ chân lông
Bước 6. Hút bã nhờn và tạp chất còn lại sâu trong lỗ chân lông
Bước 7. Thoa nước hoa hồng cân bằng da
Bước 8. Massage mặt thư giãn nâng cơ với kem matxa chiết xuất từ Nụ Tầm Xuân và Oliu
Bước 09. Phun oxy tươi, thải độc
Bước 10.Đẩy tinh chất phục hồi lô hội Azulen vera gel
Bước 11. Đắp mặt nạ cung cấp độ ẩm Oxy Gen
Bước 12. Massage hai tay giảm ngay nhức mỏi.
Bước 13. Tháo mặt nạ,cân bằng da và bôi kem dưỡng ẩm và chống nắng cho da`,
        price: 400000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2023/05/IMG20230428171151-scaled-e1685265027773-1024x937.jpg',
        product_group_id: 4,
        duration: 120,
      },
      {
        name: 'Da mụn chuyên sâu Pure Acne',
        description: `Gói điều trị mụn tại spa của chúng tôi sẽ giúp bạn giải quyết vấn đề mụn hiệu quả và tái tạo làn da khỏe mạnh. Với sự kết hợp của các liệu pháp chuyên sâu và sản phẩm chăm sóc da chất lượng, gói điều trị mụn chuẩn y khoa của chúng tôi giúp làm giảm vi khuẩn, giảm viêm và làm sạch lỗ chân lông và hạn chế mụn quay trở lại.
Làn da mịn màng, trắng sáng với 15 bước chăm sóc chuyên sâu tại Perla Spa
B1: Tẩy trang loại bỏ lớp trang điểm, bã nhờn
B2: Rửa mặt sạch sâu
B3: Tẩy da chết bằng Vitamin C peeling Gel
B4: Ủ mụn Desembre chiết xuất 12 loại thảo dược giúp tan rã, loại bỏ bã nhờn.
B5: Xông hơi, hút bã nhờn và tạp chất còn lại sâu trong lỗ chân lông
B6: Sát khuẩn cồn đỏ hoặc nước muối sinh lý.
B7: Nặn mụn bằng que nặn riêng biệt dc khử trùng sạch sẽ
B8: Điện tím sát khuẩn lại vùng da vừa nặn
B9: Đẩy Tinh chất trị mụn Desembre, chiết xuất nha đam, lá xô, chanh vàng giúp tái tạo, kháng viêm.
B10: Đắp mặt nạ tràm trà làm dịu da, kháng khuẩn, cung cấp độ ẩm cho da
B11: Trong lúc đắp mặt nạ chiếu ánh sáng sinh học kháng khuẩn, ngăn ngừa mụn
B12: Massage đầu, cổ, vai gáy thư giãn.
B13: Tháo mặt nạ, bôi nước hoa hồng cân bằng da
B14: Thoa tinh chất trị mụn giảm sẹo, chống viêm.
B15: Thoa kem chống nắng tuỳ theo tình trạng da`,
        price: 450000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2023/05/IMG202303181816051-scaled-e1685352268932-1024x1020.jpg',
        product_group_id: 4,
        duration: 180,
      },
      {
        name: 'Điện di Vitamin C trắng sáng',
        description: `Gói chăm sóc da điện di vitamin C trắng sáng là lựa chọn hoàn hảo để mang lại cho bạn làn da rạng rỡ và sáng hồng. Với sự kết hợp giữa công nghệ điện di hiện đại và vitamin C mạnh mẽ, liệu pháp này giúp làm mờ vết thâm,  làm sáng da tức thì. Trải nghiệm gói chăm sóc da điện di vitamin C trắng sáng để khôi phục và tăng cường sự tươi sáng tự nhiên của làn da, mang đến cho bạn một vẻ ngoài tươi trẻ và rạng rỡ.
1  Thăm khám da để lựa chọn dòng mỹ phẩm phù hợp
2  Tẩy trang loại bỏ lớp trang điểm, bã nhờn
3  Rửa mặt sạch sâu
4  Tẩy da chết bằng Vitamin C peeling Gel
5  Xông hơi giúp giãn nở lỗ chân lông
6  Hút bã nhờn và tạp chất còn lại sâu trong lỗ chân lông
7  Cân bằng da bằng nước hoa hồng Hydra Balance Tonic.
8  Massage mặt giúp thư giãn, lưu thông khí huyết
9  Phun oxy tươi thải độc, dưỡng ẩm
10 Đẩy tinh chất vitamin C – HA đa phân tử bằng đầu di điện từ.
11 Đắp mặt nạ Vitamin C sáng da trẻ hóa
12 Matxa đầu, cổ, vai gáy thư giãn.
13 Bỏ mặt nạ, bôi nước hoa hồng Hydra Balance
14 Thoa kem dưỡng trắng
15 Thoa kem chống nắng`,
        price: 450000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2021/11/NVD03967-scaled-e1685353475256-1024x881.jpg',
        product_group_id: 4,
        duration: 120,
      },
      {
        name: 'Trẻ hóa da Baby Skin với Mask 24k',
        description: `Gói Babyskin với mặt nạ vàng 24k tại spa mang đến cho làn da của bạn một trải nghiệm sang trọng và độc đáo. Với kết hợp của công nghệ tiên tiến và vàng 24k quý giá, liệu pháp này giúp làm sáng, cung cấp dưỡng chất và làm săn chắc da, để bạn có được làn da như da em bé. Trải nghiệm gói Babyskin với mặt nạ vàng 24k tại Perla Spa để lưu giữ tuổi thanh xuân và một làn da rực rỡ và mềm mịn.
B1: Thăm khám da để lựa chọn loại mỹ phẩm phù hợp
B2:Tẩy trang loại bỏ bụi bẩn , và lớp trang điểm trên mặt
B3: Rửa mặt giúp da sáng khoẻ
B4: Tẩy da chết loại bỏ lớp sừng trên da giúp làm sạch sâu
B5: Xông hơi thư giãn , làm giãn nở lỗ chân lông
B6: Hút bã nhờn, mụn đầu đen còn sâu trong lỗ chân lông
B7: Thoa nước hoa hồng cân bằng da
B8: Massage mặt thư giãn nâng cơ với kem matxa chiết xuất từ Nụ Tầm Xuân và Oliu
B9: Thoa kem tái sinh và đẩy tinh chất bằng đầu đẩy Oxyjet giúp da trẻ hóa và săn chắc
B10: Đắp mặt nạ vàng trẻ hoá vàng 24k
B11: Tháo mặt nạ, thoa nước hoa hồng
B12: Thoa kem dưỡng da, kem chống nắng (trước 18h)`,
        price: 450000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2023/05/IMG20230428170927-scaled-e1685352709220-1024x1000.jpg',
        product_group_id: 4,
        duration: 120,
      },
      {
        name: 'Liệu trình Da Pha Lê - Thải độc đa tầng - Bio Detox Solution',
        description: `Với liệu pháp Da Pha Lê Bio Detox Solution, làn da của bạn sẽ được thải độc và tái tạo một cách tự nhiên, mang lại làn da tươi mới và sáng mịn. Sử dụng công nghệ pha lê tiên tiến, liệu pháp này giúp làm sạch sâu và loại bỏ độc tố tích tụ trên da, tăng cường sự cân bằng và khả năng tái tạo của da. Bạn hãy trải nghiệm để đánh thức làn da từ bên trong, giúp da bạn trở nên tươi trẻ, sạch sẽ và rạng rỡ hơn bao giờ hết.
Perla Spa ứng dụng Công nghệ đột phá THẢI ĐỘC ĐA TẦNG và TÁI TẠO TẾ BÀO – BIO CELL TOX SOLUTION vào liệu trình “DA PHA LE”. Đem đến công dụng tối ưu cho làn da của bạn
Quy trình chi tiết được nghiên cứu kỹ càng tại Perla Spa mang lại hiệu quả nhanh chóng:
Tẩy trang loại bỏ bụi bẩn, dầu nhờn
Rửa mặt sạch sâu
Tẩy da chết
Đắp mặt nạ sinh học Bio Detox chiết xuất ngải cứu, tảo biển, tbg nhân sâm) => Giúp loại bỏ độc tố bề mặt, tang cường tuần hoàn máu cho da.
Massage nhẹ nhàng, xông hơi hút dầu
Đắp mặt nạ Bubble Tox o2 mask – thành phần sủi bọt tự nhiên gốc dầu dừa không gây khô và không gây kích ứng da, cấp ẩm dưỡng ẩm, tăng thể tích tế bào giúp da căng mọng mượt mà Bọt khí len lỏi vào lỗ chân lông cuốn sạch bụi bẩn, bã nhờn Cung cấp oxy cho da, giúp da dễ dàng thẩm thấu dưỡng chất
Massage đầu, cổ vai gáy tay thư giãn
ĐIỆN DI DƯỠNG CHẤT Tái tạo tế bào: Cung cấp dưỡng chất nuôi dưỡng da, làm sáng và tái tạo tế bào thông qua điện di tinh chất cô đặc giúp dưỡng chất thẩm thấu hiệu quả gấp 200 lần so với thoa dưỡng thông thường. Chiếu ánh sáng Blue, thanh lọc thải độc, làm mát da
Thoa kem dưỡng, kem chống nắng`,
        price: 600000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2023/05/IMG_1677738390443_1677738700656-e1685353916763-1024x914.jpg',
        product_group_id: 4,
        duration: 120,
      },
      {
        name: 'Gội dưỡng sinh Đài Loan',
        description: `Bước 1: Rửa mặt loại bỏ bụi bẩn
Bước 2: Ấn huyệt vùng mặt.
Bước 3: Matxa bằng sừng theo các đường kinh mạch trên vùng mặt đầu để lưu thông khí huyết
Bước 4: Matxa cổ, vai, gáy giảm đau nhức mỏi
Bước 5: Làm sạch da đầu bằng lược thông kinh lạc.
Bước 6: Gội xả bằng dầu gội thảo dược
Bước 7: Massage hai tay giảm ngay nhức mỏi.
Bước 8: Làm sạch tóc với nước ấm.
Bước 9: Lau khô sấy tóc
Bước 10: Sử dụng tinh chất chống rụng tóc và ngứa da đầu

Perla Spa với kỹ thuật viên có kỹ thuật massage bấm huyệt điêu luyện chuẩn “dưỡng sinh” kết hợp dòng sản phẩm trị liệu chuyên nghiệp, từng bước giúp các giác quan được đánh thức, tinh thần được refresh, mang đến cho người dùng trải nghiệm thư thái

`,
        price: 199000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2021/11/File_021-1536x1152.jpeg',
        product_group_id: 4,
        duration: 180,
      },
      {
        name: 'Gội Đạo Dược',
        description: `Bước 1: Thư giãn với tinh dầu bài độc được làm nóng.
Bước 2: Ấn huyệt vùng mặt.
Bước 3: Thả lỏng vùng đầu với thanh năng lượng chữ Y.
Bước 4: Massage và làm sạch da đầu bằng lược thông kinh lạc.
Bước 5: Sử dụng Khương Sơn và kem dưỡng tóc.
Bước 6: Xông hơi vùng đầu với gói tinh hoa thảo dược kèm túi đắp mắt giảm thâm quầng.
Bước 7: Massage hai tay giảm ngay nhức mỏi.
Bước 8: Làm sạch tóc với nước ấm.
Bước 9: Gội tinh chất chiết xuất từ gói Tinh hoa thảo dược.
Bước 10: Lau khô tóc, ấn huyệt vai gáy.
Bước 11: Sử dụng tinh chất giữ ẩm da đầu.
Bước 12: Lau và sấy khô tóc.
`,
        price: 350000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2020/10/goi-dau-dao-duoc-1-e1685335420674.jpg',
        product_group_id: 4,
        duration: 120,
      },
      {
        name: 'Massage Body 60 phút',
        description: `Lợi ích của massage body:
Giảm đau mỏi vai gáy
Giảm đau mỏi toàn thân
Giảm stress hay áp lực từ cuộc sống
Lưu thông khí huyết, lưu thông tuần hoàn máu
Dễ đi vào giấc ngủ
Thư giãn, sảng khoái
Hạnh phúc, yêu đời hơn
`,
        price: 350000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2023/05/IMG_1677581779489_1677585403852-e1685346917277-1024x945.jpg',
        product_group_id: 4,
        duration: 120,
      },
      {
        name: 'Massage Body 90 phút',
        description: `Bạn sẽ được ngâm chân nóng chiết xuất từ gừng giúp làm ấm lòng bàn chân và lưu thông khí huyết thải độc.
KTV sẽ giúp bạn nằm úp và thực hiện các động tác khởi động từ chân lên đầu để thư giãn cơ thể
Bằng các thao tác Massage chuyên nghiệp của Kỹ Thuật viên, tác động tới vùng đầu, cổ vai gáy, vùng lưng trên, lưng giữa, lưng dưới, 2 chân, 2 tay và vùng bụng. Toàn cơ thể được thả lỏng và thư giãn, máu huyết lưu thông giúp cho tuần hoàn máu não và loại bỏ hoàn toàn căng thẳng và mệt mỏi.
Bạn sẽ nằm ngửa để massage mặt ngửa còn lại của toàn bộ cơ thể.
Kết thúc liệu trình lau khăn nóng và thưởng trà
`,
        price: 400000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2023/09/364775333_699522812190145_60614546180689714_n-1024x1024.jpg',
        product_group_id: 4,
        duration: 180,
      },
      {
        name: 'Massage Body + Gội Đạo Dược + Chăm sóc da Glowyskin/Vitamin C 210p',
        description: `Bước 1: Ngân châm bằng gói thảo dược giúp làm nóng cơ thể
Bước 2: Nằm úp massage lưng
Bước 3: Đả thông kinh lạc cổ vai gáy bằng sừng sau đó dùng tay tác động sâu vào các điểm tắc nghẽn
Bước 4: Massage đầu, cổ, vai, gáy chân và tay
Bước 5: Masage đá nóng cổ vai gáy giúp lưu thông khí huyết
Bước 6: Tẩy trang loại bỏ các bụi bẩn và dầu thừa trên da mặt
Bước 7: Rửa mặt giúp da sáng khoẻ sạch sâu
Bước 8: Tẩy da chết loại bỏ lớp sừng trên da, giúp da sáng
Bước 9: Xông hơi giúp giãn nở lỗ chân lông. Hút bã nhờn và tạp chất còn lại sâu trong lỗ chân lông
Bước 10: Thoa nước hoa hồng cân bằng da
Bước 11: Hít tinh dầu bài độc để thư giãn và lưu thông hệ hô hấp
Bước 12: Đi thanh năng lượng chữ Y đả thông các đường kinh lạc và đánh tan những điểm tắc nghẽn
Bước 13: Massage mặt thư giãn nâng cơ với kem matxa chiết xuất từ Nụ Tầm Xuân và Oliu
Bước 14: Lau mặt bằng khăn ấm thư giãn
Bước 15: Thoa kem tái sinh Casmara giúp trẻ hoá và săn chắc da
Bước 16: Đắp mặt nạ vàng 24k Casmara
Bước 17: Chải thảo dược giúp loại bỏ bã nhờn, bụi bẩn trên da đầu
Bước 18: Gội đầu sạch sâu
Bước 19: Xông hơi và tẩy da chết vùng đầu
Bước 20: Masage tay giảm ngay nhức mỏi
Bước 21: Massage đầu
Bước 22: Xả tóc với nước ấm. Lau người bằng khăn ấm
Bước 23: Tháo mặt nạ và bôi kem dưỡng da, kem chống nắng (trước 18h)
Bước 24: Đỡ khách ngồi dậy xoa bóp tinh dầu vào cổ vai gáy
Bước 25: Sấy khô tóc và sử dụng tinh chất dưỡng tóc
`,
        price: 799000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2022/09/Beige-Modern-Aesthetic-Spring-Spa-Service-Instagram-Post-1024x1024.jpg',
        product_group_id: 4,
        duration: 180,
      },
      {
        name: 'Gội Đạo Dược + Babyskin với Mask 24k 150p',
        description: `Bước 1 Ngâm chân thảo dược và matxa chân
Bước 2 Massage cổ vai gáy giảm đau nhức
Bước 3 Massge bằng đá nóng thư giãn lưu thông hệ khí huyết
Bước 4 Tẩy trang loại bỏ bụi bẩn , và lớp trang điểm trên mặt
Bước 5 Rửa mặt giúp da sáng khoẻ
Bước 6 Tẩy da chết loại bỏ lớp sừng trên da giúp làm sạch sâu
Bước 7 Xông hơi thư giãn , làm giãn nở lỗ chân lông
Bước 8 Hút bã nhờn, mụn đầu đen còn sâu trong lỗ chân lông
Bước 9 Thoa nước hoa hồng cân bằng da
Bước 10 Hít tinh dầu bài độc giúp lưu thông hệ hô hấp,thư giãn hệ thần kinh
Bước 11 Ấn huyệt và Đi thanh năng lượng đả thông sâu vào các đường kinh lạc và các điểm tắc nghẽn trên vùng mặt và vùng đầu
Bước 12 Massage mặt thư giãn nâng cơ với kem matxa chiết xuất từ Nụ Tầm Xuân và Oliu
Bước 13 Thoa tinh chất vàng 24k và đẩy tinh chất bằng đầu đẩy Oxyjet giúp da trẻ hóa và săn chắc
Bước 14 Đắp mặt nạ vàng trẻ hoá 24k
Bước 15 Chải thảo dược vùng đầu và vai
Bước 16 Gội đầu và chải lược đả thông kinh lạc
Bước 17 Tẩy tế bào chết vùng đầu giúp loại bỏ lớp sừng và bã nhờn và cấp ẩm nuôi dưỡng đuôi tóc
Bước 18 Xông hơi nóng thư giãn vùng đầu
Bước 19 Massage hai tay giảm đau mỏi
Bước 20 Bóc mặt nạ và thoa kem dưỡng, kem chống nắng
Bước 21 Xả sạch tóc bằng nước ấm
Bước 22 Dùng khăn ấm lau cổ,vai,gáy và tay
Bước 23 Ngồi dậy massage vai cổ gáy bằng tinh dầu xoa bóp
Bước 24 xịt tinh dầu dưỡng đuôi tóc sau đó sấy khô rồi xịt dưỡng da đầu
`,
        price: 599000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2022/09/15.png',
        product_group_id: 4,
        duration: 120,
      },
      {
        name: 'Gội dưỡng sinh + Phục hồi cấp ẩm GlowySkin 120p',
        description: `Bước 1 Ngâm chân thảo dược và massage chân
Bước 2 Massage cổ vai gáy giảm đau nhức tối đa
Bước 3 Massage cổ vai gáy bằng đá nóng thư giãn lưu thông khí huyết
Bước 4 Tẩy trang loại bỏ bụi bần, dầu thừa và lớp trang điểm
Bước 5 Rửa mặt giúp da sáng khoẻ sạch sâu
Bước 6 Tẩy da chết loại bỏ lớp sừng trên da, giúp da sáng
Bước 7 Xông hơi thư giãn, giúp giãn nở lỗ chân lông
Bước 8 Hút bã nhờn và tạp chất còn lại sâu trong lỗ chân lông
Bước 9 Thoa nước hoa hồng cân bằng da
Bước 10 Massage bằng sừng theo các đường kinh mạch trên vùng mặt đầu để lưu thông khí huyết, bấm huyệt
Bước 11 Massage bằng tay nâng cơ, xoá nhăn
Bước 12 Lau khăn ấm thư giãn
Bước 13 Phun oxy tươi, thải độc
Bước 14 Đẩy tinh chất phục hồi lô hội Azulen vera gel
Bước 15 Đắp mặt nạ cung cấp độ ẩm Oxy Gen
Bước 16 Gội đầu với dầu gội cấp ẩm
Bước 17 Làm sạch da đầu bằng lược thông kinh lạc.
Bước 18 Gội xả sạch da đầu và nuôi dưỡng tóc
Bước 19 Massage hai tay giảm ngay nhức mỏi.
Bước 20 Tháo mặt nạ,cân bằng da và bôi kem dưỡng ẩm và chống nắng cho da
Bước 21 Xả tóc với nước ấm và massage da đầu
Bước 22 Lau người bằng khăn ấm giúp thư giãn, xoa bóp tinh dầu ấm nóng vai cổ gáy
Bước 23 Sấy khô tóc và sử dụng tinh chất chống rụng tóc
`,
        price: 499000,
        unit: ProductUnitsEnum.ITEM,
        img: 'https://perlaspa.com.vn/wp-content/uploads/2022/09/13.png',
        product_group_id: 4,
        duration: 120,
      },
    ]);
    await connection
      .createQueryBuilder()
      .insert()
      .into(ProductsEntity)
      .values(valuesWithDatetime)
      .execute();
  }
}
