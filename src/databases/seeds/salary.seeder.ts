import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { SalariesEntity } from '../../entity';
import { wrapValueWithDatetime } from '../utils/wrapValuesWithDatetime';

export default class CreateSalariesEntity implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const valuesWithDatetime: SalariesEntity[] = await wrapValueWithDatetime([
      // {
      //   id: 1,
      //   employee_id: 'Room 1',
      //   shift: '',
      // },
      // {
      //   id: 2,
      //   employee_id: 'Room 1',
      //   shift: '',
      // },
      // {
      //   id: 3,
      //   employee_id: 'Room 1',
      //   shift: '',
      // },
    ]);
    await connection
      .createQueryBuilder()
      .insert()
      .into(SalariesEntity)
      .values(valuesWithDatetime)
      .execute();
  }
}
