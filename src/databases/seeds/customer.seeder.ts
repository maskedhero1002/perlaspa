import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { CustomersEntity } from '../../entity';
import { wrapValueWithDatetime } from '../utils/wrapValuesWithDatetime';
import { UserGendersEnum } from '@common/enum/user.enum';

export default class CreateCustomers implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const valuesWithDatetime: CustomersEntity[] = await wrapValueWithDatetime([
      {
        id: 1,
        name: 'Daley Savage',
        phone_number: '0835500818',
        gender: UserGendersEnum.MALE,
        email: 'daleysavage124@gmail.com',
        password:
          '$2a$10$QluPhq1CI46JkrnSi0/ep.p/QzOzbYjMLSIu2prTG76cJzzgTWcUC', //changeme
        address:
          ' 88/3 Ba Huyen Thanh Quan St., Ward 9, Dist. 3 Ho Chi Minh City',
      },
      {
        id: 2,
        name: 'Hazel Jenning',
        phone_number: '0837505808',
        gender: UserGendersEnum.MALE,
        email: 'hazeljenning212@gmail.com',
        password:
          '$2a$10$QluPhq1CI46JkrnSi0/ep.p/QzOzbYjMLSIu2prTG76cJzzgTWcUC', //changeme
        address:
          '559 Dien Bien Phu Street, Ward 1, District 3 Ho Chi Minh City',
      },
      {
        id: 3,
        name: 'Malinda Herman',
        phone_number: '0839430530',
        gender: UserGendersEnum.FEMALE,
        email: 'malindaherman4124@gmail.com',
        password:
          '$2a$10$QluPhq1CI46JkrnSi0/ep.p/QzOzbYjMLSIu2prTG76cJzzgTWcUC', //changeme
        address:
          ' 551/12 Minh Phung Street, Ward 10, District 11 Ho Chi Minh City',
      },
    ]);
    await connection
      .createQueryBuilder()
      .insert()
      .into(CustomersEntity)
      .values(valuesWithDatetime)
      .execute();
  }
}
