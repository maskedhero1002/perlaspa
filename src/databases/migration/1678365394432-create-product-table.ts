import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class createProductTable1678365394432 implements MigrationInterface {
  name = 'createProductTable1678365394432';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'products',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false,
          },
          {
            name: 'description',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'img',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'price',
            type: 'int(100)',
            isNullable: false,
          },
          // {
          //   name: 'unit',
          //   type: 'enum',
          //   isNullable: false,
          //   enum: ['time', 'item'],
          // },
          {
            name: 'duration',
            type: 'int(100)',
            isNullable: true,
          },
          {
            name: 'product_group_id',
            type: 'int(100)',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'datetime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'datetime',
            default: 'now()',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('products');
  }
}
