import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class createTransactionTable1678827512736 implements MigrationInterface {
  name = 'createTransactionTable1678827512736';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'transactions',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'product_id',
            type: 'int(100)',
            isNullable: false,
          },
          {
            name: 'customer_id',
            type: 'int(100)',
            isNullable: false,
          },
          {
            name: 'room_id',
            type: 'int(100)',
            isNullable: false,
          },
          {
            name: 'hour',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'min',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'book_date',
            type: 'datetime',
            isNullable: true,
          },
          {
            name: 'status',
            type: 'enum',
            isNullable: false,
            enum: ['pending', 'submitted'],
            default: '"pending"',
          },
          {
            name: 'created_at',
            type: 'datetime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'datetime',
            default: 'now()',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('transactions');
  }
}
