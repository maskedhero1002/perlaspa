import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class createEmployeeTable1677865594087 implements MigrationInterface {
  name = 'createEmployeeTable1677865594087';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'employees',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'name',
            type: 'varchar(255)',
            isNullable: false,
          },
          {
            name: 'email',
            type: 'varchar(255)',
            isNullable: false,
          },
          {
            name: 'phone_number',
            type: 'varchar(255)',
            isNullable: false,
          },
          {
            name: 'gender',
            type: 'enum',
            isNullable: false,
            enum: ['male', 'female', 'others'],
          },
          {
            name: 'address',
            type: 'varchar(255)',
            isNullable: false,
          },
          {
            name: 'salary_range',
            type: 'int(100)',
            isNullable: false,
          },
          {
            name: 'created_at',
            type: 'datetime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'datetime',
            default: 'now()',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('employees');
  }
}
