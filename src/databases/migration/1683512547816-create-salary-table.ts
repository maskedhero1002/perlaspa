import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class createSalariesTable1683512547816 implements MigrationInterface {
  name = 'createSalariesTable1683512547816';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'salaries',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'employee_id',
            type: 'int(100)',
            isNullable: false,
          },
          {
            name: 'shift',
            type: 'enum',
            isNullable: false,
            enum: ['1', '2'],
          },
          {
            name: 'checkin_date',
            type: 'datetime',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'datetime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'datetime',
            default: 'now()',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('salaries');
  }
}
