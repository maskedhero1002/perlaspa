import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import * as hbs from 'express-handlebars';
import { capitalizeText } from '@utils/capitalize';
import { ProductGroups } from '@entity/productGroup.entity';
import { Rooms } from '@entity/room.entity';

export const hbsInitPlugin = (app: NestExpressApplication) => {
  app.setBaseViewsDir(join(__dirname, '../../..', 'views'));
  app.engine(
    'hbs',
    hbs.engine({
      extname: 'hbs',
      defaultLayout: 'main',
      layoutsDir: join(__dirname, '../../..', 'views', 'layouts'),
      partialsDir: join(__dirname, '../../..', 'views', 'partials'),
      helpers: {
        section: function (name, options) {
          if (!this._sections) this._sections = {};
          this._sections[name] = options.fn(this);
          return null;
        },
        capitalize: function (string: string) {
          return capitalizeText(string);
        },
        renderCaret: function (operand_1: any) {
          if (parseFloat(operand_1) >= 0) {
            return `<i class="fa fa-caret-up growth-rate-up"></i>`;
          } else {
            return `<i class="fa fa-caret-down growth-rate-down"></i>`;
          }
        },
        json: function (context: any) {
          return JSON.stringify(context);
        },
        renderProductGroupOptions: function (productGroups: ProductGroups[]) {
          let html = '';
          productGroups.forEach((i) => {
            html += `<option value="${i.id}">${i.name}</option>`;
          });
          return html;
        },
        renderTimeBlocks: function (room: Rooms) {
          let html = '';
          const maxDate = new Date();
          maxDate.setHours(22);
          maxDate.setMinutes(0);
          const currentDate = new Date();
          currentDate.setHours(room.timeStartHour);
          currentDate.setMinutes(room.timeStartMin);
          while (currentDate <= maxDate) {
            html += `<div class="time-block" data-roomname="${
              room.name
            }" data-hour="${currentDate.getHours()}" data-min="${currentDate.getMinutes()}" data-roomid='${
              room.id
            }'>${currentDate.getHours()}h${
              currentDate.getMinutes() != 0
                ? currentDate.getMinutes()
                : currentDate.getMinutes().toString() + '0'
            }</div>`;
            currentDate.setHours(currentDate.getHours() + 2);
          }
          return html;
        },
      },
    }),
  );
  app.setViewEngine('hbs');
};
