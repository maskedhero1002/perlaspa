export const sideMenuConfig = [
  {
    label: 'Bảng điều khiển',
    href: '/index',
    icon: 'ti-home sidemenu-icon',
    hasSubMenu: false,
    hasBadge: false,
    badge: 0,
    badgeColor: '',
    subMenu: [],
  },
  {
    label: 'Khách hàng',
    href: '/customers/list',
    icon: 'ti-user sidemenu-icon',
    hasSubMenu: false,
  },
  {
    label: 'Quản trị viên',
    href: '/admins/list',
    icon: 'fas fa-user-cog sidemenu-icon',
    hasSubMenu: false,
  },
  {
    label: 'Nhân viên',
    href: '/employees/list',
    icon: 'fas fa-user-cog sidemenu-icon',
    hasSubMenu: false,
  },
  {
    label: 'Dịch vụ',
    href: '/products/list',
    icon: 'fas fa-folder-open sidemenu-icon',
    hasSubMenu: false,
  },
  {
    label: 'Nhóm dịch vụ',
    href: '/product-group/list',
    icon: 'fas fa-folder-open sidemenu-icon',
    hasSubMenu: false,
  },
  {
    label: 'Phòng',
    href: '/rooms/list',
    icon: 'fas fa-folder-open sidemenu-icon',
    hasSubMenu: false,
  },
  {
    label: 'Quản lý đặt phòng',
    href: '/rooms/book-manage',
    icon: 'fe fe-file-text sidemenu-icon',
    hasSubMenu: false,
  },
];
