export const sideMenuUserConfig = [
  {
    label: 'Dashboard',
    href: '/index',
    icon: 'ti-home sidemenu-icon',
    hasSubMenu: false,
    hasBadge: false,
    badge: 0,
    badgeColor: '',
    subMenu: [],
  },
  {
    label: 'Services',
    href: '/products/views',
    icon: 'ti-user sidemenu-icon',
    hasSubMenu: false,
  },
  {
    label: 'Booking',
    href: '/rooms/booking',
    icon: 'fas fa-user-cog sidemenu-icon',
    hasSubMenu: false,
  },
];
