export enum SalaryShiftEnum {
  MORNING_SHIFT = 1,
  AFTERNOON_SHIFT = 2,
}
