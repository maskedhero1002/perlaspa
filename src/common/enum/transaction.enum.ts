export enum TransactionsStatusEnum {
  PENDING = 'pending',
  SUBMITTED = 'submitted',
}
