export enum UserGendersEnum {
  MALE = 'male',
  FEMALE = 'female',
  OTHERS = 'others',
}
