const dataTable = $('#chains_data').DataTable({
  processing: true,
  serverSide: true,
  searching: false,
  serverMethod: 'get',
  columnDefs: [
    {
      targets: '_all',
      orderSequence: ['desc', 'asc'],
    },
  ],
  ajax: {
    url: '/chains/datatable',
    type: 'get',
    data: function (d) {
      d.filter = {
        [$('#select-filter-field').find(':selected').val()]: $('#filters-input')
          .val()
          .toLowerCase(),
      };
    },
  },
  aaSorting: [],
  columns: [
    { data: 'id' },
    { data: 'name' },
    { data: 'chainId' },
    { data: 'image' },
    { data: 'type' },
    { data: 'showing' },
    { data: 'actions' },
  ],
});

$('#btn-search-by-filters').on('click', (e) => {
  dataTable.draw();
});

$(document).on('click', '.open-create-modal', (e) => {
  const apiUrl = '/chains/modal-create';
  helper.show(apiUrl, 'Create Chain');
});

$(document).on('click', '.open-update-modal', (e) => {
  const clickedElement = $(e.target);
  const id = clickedElement.data('id');
  const apiUrl = '/chains/modal-update' + `/${id}`;
  helper.show(apiUrl, 'Update Chain');
});

$(document).on('submit', '#create-chain-form', (e) => {
  e.preventDefault();
  formHelper.postFormJson(
    'create-chain-form',
    (data) => {
      dialog.close();
      displayToast(data.status, data.message);
      dataTable.draw();
    },
    (errors) => {
      const erorrsJSON = errors.responseJSON;
      if (erorrsJSON.status == 'error') {
        dialog.close();
        displayToast(erorrsJSON.status, erorrsJSON.message);
      } else {
        renderErrorFormException(JSON.parse(erorrsJSON));
      }
    },
  );
});

const loadFile = function (event) {
  const reader = new FileReader();
  reader.onload = function () {
    const id = event.target.getAttribute('id');
    const csrf = $('meta[name="_csrf"]');
    btn_loading.loading(id);

    helperImage.upload('/upload/image', event, csrf, (file) => {
      btn_loading.hide(id);
      if (!file) {
        toastr.error('Upload image fail!');
        event.target.value = null;
        return;
      }

      const imgId = event.target.getAttribute('img-id');
      const output = document.getElementById(imgId);
      const idUrlFile = event.target.getAttribute('id-url-file');
      const input = document.getElementById(idUrlFile);

      output.src = reader.result;
      input.value = file;
    });
  };
  reader.readAsDataURL(event.target.files[0]);
};

$('body').on('click', '.switch-button', function (e) {
  e.preventDefault();
  const arrId = $(this).attr('data-id');
  const url = '/chains/update-status-showing';
  $.confirm({
    title: 'Confirm!',
    content: 'Are you sure want to update ?',
    icon: 'fas fa-exclamation-triangle',
    type: 'red',
    autoClose: 'cancel|5000',
    buttons: {
      confirm: {
        btnClass: 'btn-red',
        text: 'Confirm',
        action: function () {
          update_showing_status(arrId, url);
        },
      },
      cancel: {
        text: 'Cancel',
      },
    },
  });
});

function update_showing_status(id, url) {
  $.ajax({
    method: 'POST',
    url: url,
    type: 'json',
    data: { id: id },
    success: function (response) {
      displayToast(response.status, response.message);
      if (response.status == 'success') {
        dataTable.draw();
      }
    },
    error: function (xhr, ajaxOptions, thrownError) {
      toastr.error(xhr.responseJSON.message);
    },
  });
}
